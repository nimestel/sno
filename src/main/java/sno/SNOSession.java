package sno;

import org.apache.wicket.Session;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.request.Request;
import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextImpl;
import sno.db.dto.AccountDTO;

public class SNOSession  extends AbstractAuthenticatedWebSession {

    /**
     * Экземпляр SecurityContext для хранения данных аутентификации
     * Создается в сессии, так как идти стандартным путем, реализованным в библиотеке
     * (отдельный экземпляр в каждом потоке) нельщя
     */
    private SecurityContext securityContext = new SecurityContextImpl();

    private AccountDTO currentUser;

    public SNOSession(Request request) {
        super(request);
        Injector.get().inject(this);
    }

    /**
     * Возвращает список ролей пользователя в текущей сессии
     */
    @Override
    public Roles getRoles() {
        Roles roles = new Roles();
        getRolesIfSignedIn(roles);
        return roles;
    }

    public static SNOSession get() {
        return (SNOSession) Session.get();
    }

    public static SecurityContext getSecurityContext() {
        return get().securityContext;
    }

    /**
     * Заполняет список ролей пользователя в текущей сессии
     * @param roles Список для заполнения
     */
    private void getRolesIfSignedIn(Roles roles) {
        if (isSignedIn()) {
            Authentication authentication = securityContext.getAuthentication();
            for (GrantedAuthority grantedAuthority : authentication.getAuthorities()) {
                roles.add(grantedAuthority.getAuthority());
            }
        }
    }

    /**
     * @return true, если пользователь успешно выполнил вход
     */
    @Override
    public boolean isSignedIn() {
        Authentication authentication = securityContext.getAuthentication();
        return authentication != null && !(authentication instanceof AnonymousAuthenticationToken) && authentication.isAuthenticated();
    }

    public String getUsername() {
        Authentication authentication = securityContext.getAuthentication();
        return (String) authentication.getPrincipal();
    }

    public AccountDTO getCurrentUser() {
        return currentUser;
    }

    public void setCurrentUser(AccountDTO currentUser) {
        this.currentUser = currentUser;
    }

    public static boolean isAdmin() {
        AccountDTO accountDTO = get().getCurrentUser();
        if (accountDTO == null) return false;

        AccountDTO.AccessLevel accessLevel = accountDTO.getAccessLevel();
        return accessLevel == AccountDTO.AccessLevel.ADMIN;
    }

}
