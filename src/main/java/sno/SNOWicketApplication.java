package sno;

import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.authroles.authentication.AbstractAuthenticatedWebSession;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import sno.web.page.login.SignInPage;
import sno.web.page.login.UserDispatcherPage;
import sno.web.page.student.StudentsPage;

public class SNOWicketApplication extends AuthenticatedWebApplication {

    @Override
    public RuntimeConfigurationType getConfigurationType() {
        return RuntimeConfigurationType.DEPLOYMENT;
    }

    @Override
	public Class<? extends WebPage> getHomePage() {
		return UserDispatcherPage.class;
	}

	@Override
	public void init() {
		super.init();

		getComponentInstantiationListeners().add(new SpringComponentInjector(this));
		getMarkupSettings().setDefaultMarkupEncoding("UTF-8");
	}

	@Override
	protected Class<? extends AbstractAuthenticatedWebSession> getWebSessionClass() {
		return SNOSession.class;
	}

	/**
	 * Вызывается, когда пользователь не аутентифицирован и пытается зайти на защищенную страницу
	 * @return Класс страницы входа
	 */
	@Override
	protected Class<? extends WebPage> getSignInPageClass() {
		return SignInPage.class;
	}

}
