package sno.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;
import sno.db.dao.SystemDAO;
import sno.db.dto.AccountDTO;
import sno.util.CommonUtils;

import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Component("authenticationProvider")
public class SNOAuthenticationProvider implements AuthenticationProvider {

    @Autowired private SystemDAO systemDAO;

    // TODO Сделать лучше. Константы соответсвуют набору ролей (см. AccountDTO.AccessLevel)
    public static final String ADMIN = "ADMIN";
    public static final String STUDENT = "STUDENT";

    /**
     * Аутентификация пользователя
     * @param authentication Экземпляр Authentication с заполненными principal и credentials (логин и пароль)
     * @return Экземпляр Authentication со списком ролей
     * @throws AuthenticationException При неуспешной аутентификации
     */
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        String username = (String) authentication.getPrincipal();
        String password = (String) authentication.getCredentials();

        if (username.isEmpty() || password.isEmpty()) {
            throw new BadCredentialsException("Имя пользователя или пароль пусты");
        } else {

            String passwordHash = null;
            try {
                passwordHash = CommonUtils.generateMD5(password);
            } catch (NoSuchAlgorithmException ignore) {}
            if (!systemDAO.authenticate(username, passwordHash)) {
                throw new BadCredentialsException("Неверная комбинация логина и пароля");
            } else {
                // TODO Это тут лишнее, надо только роль получить
                AccountDTO accountDTO = systemDAO.getAccountByUsername(username);
                List<GrantedAuthority> grantedAuthorityList = new ArrayList<>();
                grantedAuthorityList.add(new SimpleGrantedAuthority(accountDTO.getAccessLevel().name()));
                return new UsernamePasswordAuthenticationToken(username, password, grantedAuthorityList);
            }
        }
    }

    @Override
    public boolean supports(Class<?> aClass) {
        return aClass.equals(UsernamePasswordAuthenticationToken.class);
    }

}
