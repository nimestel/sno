package sno.web.page.login;

import sno.web.page.base.BasePage;

public class SignInPage extends BasePage {

    @Override
    protected void onInitialize() {
        super.onInitialize();
        add(new SignInPanel("signIn"));
    }

}
