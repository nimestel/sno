package sno.web.page.login;

import org.apache.wicket.Page;
import org.apache.wicket.RestartResponseException;
import org.apache.wicket.authroles.authentication.pages.SignInPage;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import sno.SNOSession;
import sno.db.dto.AccountDTO;
import sno.db.dto.StudentDTO;
import sno.security.SNOAuthenticationProvider;
import sno.web.page.base.BasePage;
import sno.web.page.student.StudentEditPage;
import sno.web.page.student.StudentsPage;

/**
 * Используется в качестве начальной страницы. В зависимости от прав доступа
 * перенаправляет:
 * - администратора - на страницу со списком студентов;
 * - студента - на страницу студента.
 * В ProtectedPage при отображении вкладок меню учитываются права пользователей.
 */
@AuthorizeInstantiation({SNOAuthenticationProvider.ADMIN, SNOAuthenticationProvider.STUDENT})
public class UserDispatcherPage extends BasePage {

    public UserDispatcherPage() {
        super();
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        AccountDTO.AccessLevel accessLevel = SNOSession.get().getCurrentUser().getAccessLevel();
        Page nextPage;
        switch (accessLevel) {
            case ADMIN:
                nextPage = new StudentsPage();
                break;
            case STUDENT:
                nextPage = new StudentEditPage(new StudentDTO(), this); // TODO <----
                break;
            default:
                // Сюда придти не должно, Wicket Auth/Roles не должен пустить сюда
                // без аутентификации, а роль у пользователя всегда есть.
                nextPage = new SignInPage();
        }

        throw new RestartResponseException(nextPage);
    }
}
