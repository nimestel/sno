package sno.web.page.login;

import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.StatelessForm;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import sno.SNOSession;
import sno.db.dao.SystemDAO;
import sno.db.dto.AccountDTO;

public class SignInPanel extends Panel {

    private String username;
    private String password;

    @SpringBean(name = "authenticationProvider")
    private AuthenticationProvider authenticationProvider;

    @SpringBean(name = "systemDAO")
    private SystemDAO systemDAO;

    @SuppressWarnings("unchecked")
    public SignInPanel(final String id) {
        super(id);

        SignInForm signInForm = new SignInForm("signInForm");
        signInForm.add(new FeedbackPanel("feedback"));
        add(signInForm);
    }

    /**
     * Форма для аутентификации
     */
    public final class SignInForm extends StatelessForm<SignInPanel> {

        public SignInForm(final String id) {
            super(id);

            setModel(new CompoundPropertyModel<>(SignInPanel.this));

            add(new TextField<String>("username").setLabel(Model.of("Логин"))
                    .setRequired(true));
            add(new PasswordTextField("password").setLabel(Model.of("Пароль")));
        }

        @Override
        @SuppressWarnings("unchecked")
        public void onSubmit() {
            try {
                Authentication authentication = new UsernamePasswordAuthenticationToken(username, password);
                authentication = authenticationProvider.authenticate(authentication);

                // При успешной аутентификации она запоминается в сесии, пользователя перекидывает на
                // домашнюю страницу ( и в этот раз его туда пустит)
                SNOSession.getSecurityContext().setAuthentication(authentication);
                AccountDTO accountDTO = systemDAO.getAccountByUsername((String) authentication.getPrincipal());
                SNOSession.get().setCurrentUser(accountDTO);

                setResponsePage(getApplication().getHomePage());
            } catch (Exception e) {
                error(e.getMessage());
            }
        }
    }

}