package sno.web.page.base;

import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.markup.head.IHeaderResponse;
import org.apache.wicket.markup.head.OnDomReadyHeaderItem;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.Model;
import sno.SNOWicketApplication;
import sno.SNOSession;
import sno.web.page.student.EventsPage;
import sno.web.page.student.StudentsPage;

/**
 * От этой страницы наследуются все страницы проекта, доступ к которым
 * разрешен только после аутентификации.
 * Здесь же рисуется верхнее меню (включая панель пользователя).
 */
public abstract class ProtectedPage extends BasePage {

    public static final String STUDENT_TAB = "studentsTab";
    public static final String EVENTS_TAB = "eventsTab";

    /**
     * Класс вкладки верхнего меню.
     * Отличается от простой ссылки тем, что если вкладка уже выбрана,
     * то на клик нет никакой реакции.
     */
    private class MenuItemLink extends AjaxFallbackLink {

        private Class<? extends Page> linkPageClass;

        public MenuItemLink(String id, Class<? extends Page> linkPageClass) {
            super(id);
            this.linkPageClass = linkPageClass;
        }

        @Override
        public void onClick(AjaxRequestTarget ajaxRequestTarget) {
            if (getId().equals(ProtectedPage.this.getSelectedMenuItemClass())) return;
            else setResponsePage(linkPageClass);
        }
    }

    public ProtectedPage() {
        super();
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        // Добавление вкладок верхнего меню
        // Вкладки меню отображаются только для администраторов.
        add(new MenuItemLink("studentsTab", StudentsPage.class) {
            @Override
            public boolean isVisible() {
                return super.isVisible() && SNOSession.isAdmin();
            }
        });
        add(new MenuItemLink("eventsTab", EventsPage.class) {
            @Override
            public boolean isVisible() {
                return super.isVisible() && SNOSession.isAdmin();
            }
        });

        // Панель пользователя
        add(new Label("username", Model.of(SNOSession.get().getUsername())));
        add(new AjaxFallbackLink("logout") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                SNOSession.get().setCurrentUser(null);
                SNOSession.get().invalidateNow();
                setResponsePage(SNOWicketApplication.get().getHomePage());
            }
        });
    }

    @Override
    public void renderHead(IHeaderResponse response) {
        super.renderHead(response);

        // TODO Это не работает =\
        // Этот кусок кода должен выделять цветом активню вкладку верхнего меню
        String selectedItem = getSelectedMenuItemClass();
        if (selectedItem != null) {
            response.render(OnDomReadyHeaderItem.forScript(
                    "getElementByClassName('" + selectedItem + "').addClass('active')"
            ));
        }
    }

    // Если возвращается не null, то вкладка с указанным css-классом
    // помечается активной
    protected String getSelectedMenuItemClass() {
        return null;
    }
}
