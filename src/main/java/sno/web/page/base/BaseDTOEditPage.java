package sno.web.page.base;

import org.apache.wicket.ajax.AjaxRequestTarget;
import sno.db.dto.base.BaseDTO;
import sno.util.CommonUtils;

public abstract class BaseDTOEditPage<T extends BaseDTO> extends BaseEditPage {

    private T initObject;
    private T curObject;

    protected BaseDTOEditPage(T object, BasePage backPage) {
        super(backPage);

        initObject = object.getId() == null ? object : getObjectById(object.getId());
        curObject = CommonUtils.cloneObject(object);
    }

    protected abstract T getObjectById(long id);

    protected abstract void postObject(T object);

    protected abstract void deleteObject(long id);

    @Override
    protected boolean isChanged() {
        return !initObject.equals(curObject);
    }

    @Override
    protected boolean showDeleteLinks() {
        return curObject.getId() != null;
    }

    @Override
    protected void onDelete(AjaxRequestTarget ajaxRequestTarget) {
        Long id = curObject.getId();
        if (curObject.getId() != null) deleteObject(curObject.getId());
    }

    @Override
    protected void onSave(AjaxRequestTarget ajaxRequestTarget) {
        if (isChanged()) postObject(curObject);
    }

    protected T getCurObject() {
        return curObject;
    }

}
