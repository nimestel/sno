package sno.web.page.base;

import org.apache.wicket.markup.html.WebPage;

/**
 * От этой страницы должны наследоваться все страницы проекта.
 */
public abstract class BasePage extends WebPage {

    protected BasePage() {
        super();
    }

}
