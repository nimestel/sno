package sno.web.page.base;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.markup.html.WebMarkupContainer;

public abstract class BaseEditPage extends ProtectedPage {

    private BasePage backPage;
    private WebMarkupContainer goBackContainer;
    private WebMarkupContainer saveCancelContainer;

    public class EditPageUpdateBehaviour extends AjaxFormComponentUpdatingBehavior {

        public EditPageUpdateBehaviour() {
            super("change");
        }

        @Override
        protected void onUpdate(AjaxRequestTarget ajaxRequestTarget) {
            ajaxRequestTarget.add(goBackContainer, saveCancelContainer);
        }
    }

    protected BaseEditPage(BasePage backPage) {
        this.backPage = backPage;
    }

    protected abstract boolean isChanged();

    protected abstract boolean showDeleteLinks();

    protected abstract void onDelete(AjaxRequestTarget ajaxRequestTarget);

    protected abstract void onSave(AjaxRequestTarget ajaxRequestTarget);

    private class DeleteLink extends AjaxFallbackLink {

        public DeleteLink(String id) {
            super(id);
        }

        @Override
        public void onClick(AjaxRequestTarget ajaxRequestTarget) {
            onDelete(ajaxRequestTarget);
            setResponsePage(backPage);
        }

        @Override
        public boolean isVisible() {
            return super.isVisible() && showDeleteLinks();
        }
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        goBackContainer = new WebMarkupContainer("goBackPanel") {
            @Override
            protected void onConfigure() {
                super.onConfigure();
                setOutputMarkupId(true);
                setOutputMarkupPlaceholderTag(true);
            }

            @Override
            public boolean isVisible() {
                return super.isVisible() && !isChanged();
            }
        };
        add(goBackContainer);

        saveCancelContainer = new WebMarkupContainer("saveCancelPanel") {
            @Override
            protected void onConfigure() {
                super.onConfigure();
                setOutputMarkupId(true);
                setOutputMarkupPlaceholderTag(true);
            }

            @Override
            public boolean isVisible() {
                return super.isVisible() && isChanged();
            }
        };
        add(saveCancelContainer);

        goBackContainer.add(new AjaxFallbackLink("goBack") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                setResponsePage(backPage);
            }
        });
        // TODO Дублирование кнопок удаления
        goBackContainer.add(new DeleteLink("delete1"));

        saveCancelContainer.add(new AjaxFallbackLink("save") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                onSave(ajaxRequestTarget);
                setResponsePage(backPage);
            }
        });
        saveCancelContainer.add(new AjaxFallbackLink("cancel") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                setResponsePage(backPage);
            }
        });
        saveCancelContainer.add(new DeleteLink("delete2"));
    }

}
