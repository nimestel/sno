package sno.web.page.student;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sno.db.dao.UniversityStructureDAO;
import sno.db.dto.EventDTO;
import sno.db.dto.StudentDTO;
import sno.db.sc.BaseSC;
import sno.db.sc.SortingDescription;
import sno.security.SNOAuthenticationProvider;
import sno.web.component.AjaxTextTable;
import sno.web.component.RussianNamedEnumConverter;
import sno.web.component.SimpleDateField;
import sno.web.page.base.ProtectedPage;

import java.util.List;

@AuthorizeInstantiation({SNOAuthenticationProvider.ADMIN})
public class EventsPage extends ProtectedPage {

    @SpringBean
    private UniversityStructureDAO universityStructureDAO;

    @Override
    protected void onInitialize() {
        super.onInitialize();

        AjaxTextTable<EventDTO> eventsTable = new AjaxTextTable<EventDTO>("eventsTable") {
            @Override
            protected List<EventDTO> loadData(String sortingField, SortingDescription.Sorting sorting) {
                BaseSC baseSC = new BaseSC();
                baseSC.addSorting(sortingField, sorting);
                return universityStructureDAO.getEvents(baseSC);
            }

            @Override
            protected void onClick(AjaxRequestTarget target, EventDTO eventDTO) {
                setResponsePage(new EventEditPage(eventDTO, EventsPage.this));
            }
        };
        eventsTable.addColumn("Событие", "name");
        eventsTable.addColumn("Тип", "eventType", new RussianNamedEnumConverter());
        eventsTable.addColumn("Статус", "status", new RussianNamedEnumConverter());
        eventsTable.addColumn("С", "beginDate");
        eventsTable.addColumn("По", "endDate");
        eventsTable.addColumn("Регистрация с", "registrationBeginDate");
        eventsTable.addColumn("Регистрация по", "registrationEndDate");
        add(eventsTable);

        add(new AjaxFallbackLink("add") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                // TODO Добавление
            }
        });

        add(new AjaxFallbackLink("report") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                // TODO
            }
        });

        add(new SimpleDateField("fakeDate1"));
        add(new SimpleDateField("fakeDate2"));
        add(new SimpleDateField("fakeDate3"));
        add(new SimpleDateField("fakeDate4"));
    }

    @Override
    protected String getSelectedMenuItemClass() {
        return EVENTS_TAB;
    }

}
