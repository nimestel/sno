package sno.web.page.student;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.TextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sno.db.dao.UniversityStructureDAO;
import sno.db.dto.EventParticipationDTO;
import sno.db.dto.GroupDTO;
import sno.db.dto.StudentDTO;
import sno.db.sc.EventParticipationSC;
import sno.db.sc.SortingDescription;
import sno.security.SNOAuthenticationProvider;
import sno.web.component.AjaxTextTable;
import sno.web.component.NamedDTOChoiceRenderer;
import sno.web.component.RussianNamedEnumConverter;
import sno.web.component.SimpleDateField;
import sno.web.page.base.BaseDTOEditPage;
import sno.web.page.base.BasePage;
import sno.web.page.student.selection.EventSelectionPage;
import sno.web.page.student.selection.StudentInterestsPage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@AuthorizeInstantiation({SNOAuthenticationProvider.ADMIN, SNOAuthenticationProvider.STUDENT})
public class StudentEditPage extends BaseDTOEditPage<StudentDTO> {

    @SpringBean
    private UniversityStructureDAO universityStructureDAO;

    public StudentEditPage(StudentDTO object, BasePage backPage) {
        super(object, backPage);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        Form<StudentDTO> form = new Form<>("editForm", new CompoundPropertyModel<>(getCurObject()));
        form.add(new TextField<String>("lastName").add(new EditPageUpdateBehaviour()));
        form.add(new TextField<String>("firstName").add(new EditPageUpdateBehaviour()));
        form.add(new TextField<String>("patronymic").add(new EditPageUpdateBehaviour()));

        form.add(new SimpleDateField("birthDate").add(new EditPageUpdateBehaviour()));
        form.add(new SimpleDateField("beginDate").add(new EditPageUpdateBehaviour()));
        form.add(new SimpleDateField("endDate").add(new EditPageUpdateBehaviour()));

        List<GroupDTO> groupDTOList = universityStructureDAO.getGroups(null);
        form.add(new DropDownChoice<>("groupDTO", groupDTOList, new NamedDTOChoiceRenderer<>())
                .add(new EditPageUpdateBehaviour())
        );

        List<StudentDTO.Gender> genderList = Arrays.asList(StudentDTO.Gender.values());
        form.add(new DropDownChoice<>("gender", genderList, new ChoiceRenderer<>("russianName"))
            .add(new EditPageUpdateBehaviour())
        );

        form.add(new TextField<String>("snoFunctions").add(new EditPageUpdateBehaviour()));
        form.add(new TextField<String>("hobby").add(new EditPageUpdateBehaviour()));
        form.add(new TextField<String>("commentary").add(new EditPageUpdateBehaviour()));
        add(form);

        add(new AjaxFallbackLink<Void>("editInterests") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                setResponsePage(new StudentInterestsPage(getCurObject(), StudentEditPage.this));
            }
        });

        add(new AjaxFallbackLink<Void>("selectEvents") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                setResponsePage(new EventSelectionPage(getCurObject(), StudentEditPage.this));
            }
        });

        AjaxTextTable<EventParticipationDTO> eventsTable = new AjaxTextTable<EventParticipationDTO>("eventsTable") {
            @Override
            public boolean isVisible() {
                return super.isVisible() && getCurObject().getId() != null;
            }

            @Override
            protected List<EventParticipationDTO> loadData(String sortingField, SortingDescription.Sorting sorting) {
                Long studentId = getCurObject().getId();
                EventParticipationSC sc = new EventParticipationSC();
                sc.setStudentId(studentId);
                return studentId == null ? new ArrayList<>() : universityStructureDAO.getEventParticipations(sc);
            }
        };
        eventsTable.addColumn("Событие", "eventDTO.name");
        eventsTable.addColumn("Тип события", "eventDTO.eventType", new RussianNamedEnumConverter());
        eventsTable.addColumn("Статус события", "eventDTO.status", new RussianNamedEnumConverter());
        eventsTable.addColumn("Дата участия", "beginDate");
        add(eventsTable);
    }

    @Override
    protected StudentDTO getObjectById(long id) {
        return universityStructureDAO.getStudentById(id);
    }

    @Override
    protected void postObject(StudentDTO object) {
        universityStructureDAO.postStudent(object);
    }

    @Override
    protected void deleteObject(long id) {
        universityStructureDAO.deleteStudentById(id);
    }
}
