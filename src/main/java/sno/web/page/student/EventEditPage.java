package sno.web.page.student;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sno.db.dao.UniversityStructureDAO;
import sno.db.dto.EventDTO;
import sno.db.dto.EventParticipationDTO;
import sno.db.sc.EventParticipationSC;
import sno.db.sc.SortingDescription;
import sno.security.SNOAuthenticationProvider;
import sno.web.component.AjaxTextTable;
import sno.web.page.base.BaseDTOEditPage;
import sno.web.page.base.BasePage;
import sno.web.page.student.selection.EventInterestsPage;
import sno.web.page.student.selection.EventSelectionPage;
import sno.web.page.student.selection.StudentSelectionPage;

import java.util.ArrayList;
import java.util.List;

@AuthorizeInstantiation({SNOAuthenticationProvider.ADMIN})
public class EventEditPage extends BaseDTOEditPage<EventDTO> {

    @SpringBean
    private UniversityStructureDAO universityStructureDAO;

    protected EventEditPage(EventDTO object, BasePage backPage) {
        super(object, backPage);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        Form<EventDTO> form = new Form<>("editForm", new CompoundPropertyModel<>(getCurObject()));
        // TODO Редактирование события
        add(form);

        add(new AjaxFallbackLink<Void>("editDisciplines") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                setResponsePage(new EventInterestsPage(getCurObject(), EventEditPage.this));
            }
        });

        add(new AjaxFallbackLink<Void>("selectStudents") { // TODO Не показывать для зкаонченных
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                setResponsePage(new StudentSelectionPage(getCurObject(), EventEditPage.this));
            }
        });

        AjaxTextTable<EventParticipationDTO> studentsTable = new AjaxTextTable<EventParticipationDTO>("studentsTable") {
            @Override
            public boolean isVisible() {
                return super.isVisible() && getCurObject().getId() != null;
            }

            @Override
            protected List<EventParticipationDTO> loadData(String sortingField, SortingDescription.Sorting sorting) {
                Long eventId = getCurObject().getId();
                EventParticipationSC sc = new EventParticipationSC();
                sc.setEventId(eventId);
                return eventId == null ? new ArrayList<>() : universityStructureDAO.getEventParticipations(sc);
            }
        };
        studentsTable.addColumn("Фамилия", "studentDTO.lastName");
        studentsTable.addColumn("Имя", "studentDTO.firstName");
        studentsTable.addColumn("Группа", "studentDTO.groupDTO.acronym", "student__group_acronym");
        studentsTable.addColumn("Курс", "studentDTO.groupDTO.year", "student__group_year");
        add(studentsTable);
    }

    @Override
    protected EventDTO getObjectById(long id) {
        return universityStructureDAO.getEventById(id);
    }

    @Override
    protected void postObject(EventDTO object) {
        // TODO Реализовать
    }

    @Override
    protected void deleteObject(long id) {
        // TODO Реализовать
    }

}
