package sno.web.page.student;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sno.db.dao.UniversityStructureDAO;
import sno.db.dto.StudentDTO;
import sno.db.sc.BaseSC;
import sno.db.sc.SortingDescription;
import sno.security.SNOAuthenticationProvider;
import sno.web.component.AjaxTextTable;
import sno.web.page.base.ProtectedPage;

import java.util.List;

@AuthorizeInstantiation({SNOAuthenticationProvider.ADMIN})
public class StudentsPage extends ProtectedPage {

    @SpringBean
    private UniversityStructureDAO universityStructureDAO;

    @Override
    protected void onInitialize() {
        super.onInitialize();

        AjaxTextTable<StudentDTO> studentsTable = new AjaxTextTable<StudentDTO>("studentsTable") {
            @Override
            protected List<StudentDTO> loadData(String sortingField, SortingDescription.Sorting sorting) {
                BaseSC baseSC = new BaseSC();
                baseSC.addSorting(sortingField, sorting);
                return universityStructureDAO.getStudents(baseSC);
            }

            @Override
            protected void onClick(AjaxRequestTarget target, StudentDTO studentDTO) {
                setResponsePage(new StudentEditPage(studentDTO, StudentsPage.this));
            }
        };
        studentsTable.addColumn("Группа", "groupDTO.name", "_group_name");
        studentsTable.addColumn("Фамилия", "lastName");
        studentsTable.addColumn("Имя", "firstName");
        studentsTable.addColumn("Отчество", "patronymic");
        studentsTable.addColumn("Функции", "snoFunctions");
        add(studentsTable);

        add(new AjaxFallbackLink("add") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                setResponsePage(new StudentEditPage(new StudentDTO(), StudentsPage.this));
            }
        });

        add(new AjaxFallbackLink("report") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                // TODO
            }
        });
    }

    @Override
    protected String getSelectedMenuItemClass() {
        return STUDENT_TAB;
    }
}
