package sno.web.page.student.selection;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sno.db.dao.UniversityStructureDAO;
import sno.db.dto.EventDTO;
import sno.db.dto.StudentDTO;
import sno.util.SelectionUtil;
import sno.web.page.base.BasePage;
import sno.web.page.base.ProtectedPage;

import java.util.List;
import java.util.Map;

// TODO Рефактор, много общего с EventSelectionPage
public class StudentSelectionPage extends ProtectedPage {

    @SpringBean
    private UniversityStructureDAO universityStructureDAO;

    @SpringBean
    private SelectionUtil selectionUtil;

    private BasePage backPage;
    private EventDTO eventDTO;

    public StudentSelectionPage(EventDTO eventDTO, BasePage backPage) {
        this.backPage = backPage;
        this.eventDTO = eventDTO;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new AjaxFallbackLink("goBack") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                setResponsePage(backPage);
            }
        });

        add(new Label("header", "Подходящие студенты для события \"" + eventDTO.getName() + "\""));

        List<StudentDTO> studentDTOList = universityStructureDAO.getStudents(null);
        Map<Long, Double> interests = selectionUtil.calculateEventInterest(eventDTO);
        studentDTOList.sort((s1, s2) -> Double.compare(interests.get(s2.getId()), interests.get(s1.getId())));

        DataView<StudentDTO> studentDTODataView = new DataView<StudentDTO>(
                "studentsTableRow", new ListDataProvider<>(studentDTOList)
        ) {
            @Override
            protected void populateItem(Item<StudentDTO> item) {
                StudentDTO studentDTO = item.getModelObject();
                item.add(new Label("name", String.format(
                        "%s %s, %d курс %s",
                        studentDTO.getLastName(), studentDTO.getFirstName(),
                        studentDTO.getGroupDTO().getYear(), studentDTO.getGroupDTO().getAcronym()
                )));
                item.add(new Label("interest", String.format(
                        "%.2f", interests.get(studentDTO.getId())
                )));
            }
        };
        add(studentDTODataView);
    }

}
