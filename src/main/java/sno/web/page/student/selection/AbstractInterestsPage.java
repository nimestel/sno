package sno.web.page.student.selection;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.LoadableDetachableModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sno.db.dao.UniversityStructureDAO;
import sno.db.dto.DisciplineDTO;
import sno.db.dto.base.BaseDTO;
import sno.web.page.base.BaseEditPage;
import sno.web.page.base.BasePage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

abstract class AbstractInterestsPage extends BaseEditPage {

    @SpringBean
    protected UniversityStructureDAO universityStructureDAO;

    private HashMap<DisciplineDTO, Boolean> interests = new HashMap<>();
    private HashMap<DisciplineDTO, Boolean> changed = new HashMap<>();
    private int changedCount = 0;

    protected AbstractInterestsPage(BasePage basePage) {
        super(basePage);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new Label("header", Model.of(getHeader())));

        List<DisciplineDTO> disciplineDTOList = universityStructureDAO.getDisciplines();
        IDataProvider<DisciplineDTO> dataProvider = new ListDataProvider<>(disciplineDTOList);
        fillInterests(disciplineDTOList);

        DataView<DisciplineDTO> disciplineDTODataView = new DataView<DisciplineDTO>("disciplinesTableRow", dataProvider) {
            @Override
            protected void populateItem(Item<DisciplineDTO> item) {
                DisciplineDTO disciplineDTO = item.getModelObject();

                CheckBox interestCheckBox = new CheckBox("interest", new LoadableDetachableModel<Boolean>() {
                    @Override
                    protected Boolean load() {
                        Boolean res = interests.get(disciplineDTO);
                        return res != null ? res : Boolean.FALSE;
                    }
                });
                interestCheckBox.add(new EditPageUpdateBehaviour() {
                    @Override
                    protected void onUpdate(AjaxRequestTarget ajaxRequestTarget) {
                        super.onUpdate(ajaxRequestTarget);

                        interests.put(disciplineDTO, interestCheckBox.getModel().getObject());

                        Boolean changedValue = changed.get(disciplineDTO);
                        if (changedValue == null || !changedValue) {
                            changedCount++;
                        } else {
                            changedCount--;
                        }
                        changed.put(disciplineDTO, changedValue != null ? !changedValue : Boolean.TRUE);
                    }
                });

                item.add(new Label("name", Model.of(disciplineDTO.getName())));
                item.add(interestCheckBox);
            }
        };
        disciplineDTODataView.setOutputMarkupId(true);
        add(disciplineDTODataView);
    }

    private void fillInterests(List<DisciplineDTO> disciplineDTOList) {
        List<DisciplineDTO> interestingDisciplineDTOList = getInterestingDisciplines();
        for (DisciplineDTO disciplineDTO : disciplineDTOList) {
            interests.put(disciplineDTO, Boolean.FALSE);
            changed.put(disciplineDTO, Boolean.FALSE);
        }
        for (DisciplineDTO disciplineDTO : interestingDisciplineDTOList) {
            interests.put(disciplineDTO, Boolean.TRUE);
        }
    }

    protected abstract String getHeader();

    protected abstract List<DisciplineDTO> getInterestingDisciplines();

    protected abstract void deleteInterests(List<DisciplineDTO> toDelete);
    protected abstract void postInterests(List<DisciplineDTO> toPost);

    @Override
    protected boolean isChanged() {
        return changedCount > 0;
    }

    @Override
    protected boolean showDeleteLinks() {
        return false;
    }

    @Override
    protected void onDelete(AjaxRequestTarget ajaxRequestTarget) {
        // Никогда тут не будет вызвано
    }

    @Override
    protected void onSave(AjaxRequestTarget ajaxRequestTarget) {
        if (changedCount > 0) {
            List<DisciplineDTO> toDelete = new ArrayList<>();
            List<DisciplineDTO> toPost = new ArrayList<>();
            for (Map.Entry<DisciplineDTO, Boolean> entry : changed.entrySet()) {
                Boolean value = entry.getValue();
                if (value != null && value) {
                    if (interests.get(entry.getKey())) {
                        toPost.add(entry.getKey());
                    } else {
                        toDelete.add(entry.getKey());
                    }
                }
            }

            deleteInterests(toDelete);
            postInterests(toPost);
        }
    }

}
