package sno.web.page.student.selection;

import org.apache.wicket.authroles.authorization.strategies.role.annotations.AuthorizeInstantiation;
import sno.db.dto.DisciplineDTO;
import sno.db.dto.StudentDTO;
import sno.security.SNOAuthenticationProvider;
import sno.web.page.base.BasePage;

import java.util.List;

@AuthorizeInstantiation({SNOAuthenticationProvider.ADMIN, SNOAuthenticationProvider.STUDENT})
public class StudentInterestsPage extends AbstractInterestsPage {

    private StudentDTO studentDTO;

    public StudentInterestsPage(StudentDTO studentDTO, BasePage backPage) {
        super(backPage);
        this.studentDTO = studentDTO;
    }

    @Override
    protected String getHeader() {
        return String.format(
                "Интересы студента %s %s, %d курс %s",
                studentDTO.getLastName(), studentDTO.getFirstName(),
                studentDTO.getGroupDTO().getYear(), studentDTO.getGroupDTO().getAcronym()
        );
    }

    @Override
    protected List<DisciplineDTO> getInterestingDisciplines() {
        return universityStructureDAO.getInterestingDisciplinesForStudent(studentDTO);
    }

    @Override
    protected void deleteInterests(List<DisciplineDTO> toDelete) {
        universityStructureDAO.deleteInterestingDisciplinesForStudent(studentDTO, toDelete);
    }

    @Override
    protected void postInterests(List<DisciplineDTO> toPost) {
        universityStructureDAO.postInterestingDisciplinesForStudent(studentDTO, toPost);
    }
}
