package sno.web.page.student.selection;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sno.db.dao.UniversityStructureDAO;
import sno.db.dto.EventDTO;
import sno.db.dto.StudentDTO;
import sno.util.SelectionUtil;
import sno.web.page.base.BasePage;
import sno.web.page.base.ProtectedPage;

import java.util.List;
import java.util.Map;

public class EventSelectionPage extends ProtectedPage {

    @SpringBean
    private UniversityStructureDAO universityStructureDAO;

    @SpringBean
    private SelectionUtil selectionUtil;

    private BasePage backPage;
    private StudentDTO studentDTO;

    public EventSelectionPage(StudentDTO studentDTO, BasePage backPage) {
        this.backPage = backPage;
        this.studentDTO = studentDTO;
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        add(new AjaxFallbackLink("goBack") {
            @Override
            public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                setResponsePage(backPage);
            }
        });

        add(new Label("header", String.format(
                "Подходящие события для студента %s %s, %d курс %s",
                studentDTO.getLastName(), studentDTO.getFirstName(),
                studentDTO.getGroupDTO().getYear(), studentDTO.getGroupDTO().getAcronym()
        )));

        List<EventDTO> eventDTOList = universityStructureDAO.getEvents(null); // TODO Только не законченные!
        Map<Long, Double> interests = selectionUtil.calculateStudentInterest(studentDTO);
        eventDTOList.sort((e1, e2) -> Double.compare(interests.get(e2.getId()), interests.get(e1.getId())));

        DataView<EventDTO> eventDTODataView = new DataView<EventDTO>(
                "eventsTableRow", new ListDataProvider<>(eventDTOList)
        ) {
            @Override
            protected void populateItem(Item<EventDTO> item) {
                EventDTO eventDTO = item.getModelObject();
                item.add(new Label("name", eventDTO.getName()));
                item.add(new Label("interest", String.format(
                        "%.2f", interests.get(eventDTO.getId())
                )));
            }
        };
        add(eventDTODataView);
    }
}
