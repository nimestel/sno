package sno.web.page.student.selection;

import sno.db.dto.DisciplineDTO;
import sno.db.dto.EventDTO;
import sno.web.page.base.BasePage;

import java.util.List;

public class EventInterestsPage extends AbstractInterestsPage {

    private EventDTO eventDTO;

    public EventInterestsPage(EventDTO eventDTO, BasePage basePage) {
        super(basePage);
        this.eventDTO = eventDTO;
    }

    @Override
    protected String getHeader() {
        return "Дисциплины, связанные с событием \"" + eventDTO.getName() + "\"";
    }

    @Override
    protected List<DisciplineDTO> getInterestingDisciplines() {
        return universityStructureDAO.getInterestingDisciplinesForEvent(eventDTO);
    }

    @Override
    protected void deleteInterests(List<DisciplineDTO> toDelete) {
        universityStructureDAO.deleteInterestingDisciplinesForEvent(eventDTO, toDelete);
    }

    @Override
    protected void postInterests(List<DisciplineDTO> toPost) {
        universityStructureDAO.postInterestingDisciplinesForEvent(eventDTO, toPost);
    }
}
