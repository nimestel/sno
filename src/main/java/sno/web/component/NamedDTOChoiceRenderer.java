package sno.web.component;

import org.apache.wicket.markup.html.form.IChoiceRenderer;
import org.apache.wicket.model.IModel;
import sno.db.dto.base.BaseNamedCommentedDTO;

import java.util.List;

/**
 * Рендерер для генерации представления объектов BaseNamedCommentedDTO,
 * использует имя в качестве представления
 * @param <T> Класс модели
 */
public class NamedDTOChoiceRenderer<T extends BaseNamedCommentedDTO> implements IChoiceRenderer<T> {

    @Override
    public Object getDisplayValue(T t) {
        return t.getName();
    }

    @Override
    public String getIdValue(T t, int i) {
        return Integer.toString(i);
    }

    @Override
    public T getObject(String s, IModel<? extends List<? extends T>> iModel) {
        return iModel.getObject().get(Integer.parseInt(s));
    }
}
