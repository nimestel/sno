package sno.web.component;

import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxFallbackLink;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.repeater.Item;
import org.apache.wicket.markup.repeater.data.DataView;
import org.apache.wicket.markup.repeater.data.IDataProvider;
import org.apache.wicket.markup.repeater.data.ListDataProvider;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.util.convert.IConverter;
import sno.db.sc.SortingDescription;
import sno.util.CommonUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс для упрощения создания таблиц на странице.
 * Таблица будет перезагружаться при добавлении в ajaxRequestTarget,
 * позволяет повесить действие на клик по строке,
 * автоматически реализует сортировку строк по столбцу.
 *
 * Разметка должна быть следующая:
 * <table wicket:id='tableId'>
 *     <thead>
 *         <th><a wicket:id='th_column1' /></th>
 *         <th><a wicket:id='th_column2' /></th>
 *         ...
 *     </thead>
 *     <tbody wicket:id='tr_tableId'>
 *         <tr><span wicket:id='column1' /></tr>
 *         <tr><span wicket:id='column2' /></tr>
 *         ...
 *     </tbody>
 * </table>
 *
 * Тогда объект нужно создавать с идентификатором 'tableId', колонки добавлять с
 * идентифитаторами 'column1', 'column2' и т.д.
 *
 * @param <T> Класс модели строк
 */
public abstract class AjaxTextTable<T extends Serializable> extends WebMarkupContainer {

    private SortingDescription sortingDescription = null;

    // Характеризует столбец таблицы
    private class Column implements Serializable {
        public String name; // Заголовок столбца
        public String propertyName; // Имя поля модели для отображения
        public String sortColumnName; // Имя колонки БД для сортировки
        public IConverter converter; // Конвертер значения поля в строку

        public Column(String name, String propertyName, String sortColumnName) {
            this(name, propertyName, sortColumnName, null);
        }

        public Column(String name, String propertyName, String sortColumnName, IConverter converter) {
            this.name = name;
            this.propertyName = propertyName;
            this.sortColumnName = sortColumnName;
            this.converter = converter;
        }
    }

    private List<Column> columns = new ArrayList<>();

    public AjaxTextTable(String id) {
        super(id);

        IDataProvider<T> dataProvider = new ListDataProvider<T>() {
            @Override
            protected List<T> getData() {
                return sortingDescription != null ? loadData(sortingDescription.field, sortingDescription.sorting) :
                        loadData(null, null);
            }
        };

        // Заполнение строк таблицы, одновременно для каждой строки
        // добавляется событие на клик
        DataView<T> dataView = new DataView<T>("tr_" + id, dataProvider) {
            @Override
            protected void populateItem(Item<T> item) {
                T object = item.getModelObject();
                item.setModel(new CompoundPropertyModel<>(object));
                item.add(new AjaxEventBehavior("click") {
                    @Override
                    protected void onEvent(AjaxRequestTarget ajaxRequestTarget) {
                        onClick(ajaxRequestTarget, item.getModelObject());
                    }
                });

                columns.forEach(c -> item.add(new Label(c.propertyName) {
                    @Override
                    public <C> IConverter<C> getConverter(Class<C> type) {
                        return c.converter == null ? super.getConverter(type) : c.converter;
                    }
                }));
            }
        };
        dataView.setOutputMarkupId(true);
        add(dataView);

        setOutputMarkupId(true);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        // Заполнение шапки таблицы, на клик вешается сортировка;
        // если колонка уже отсортирована, меняется направление сортировки
        columns.forEach(c -> {
            AjaxFallbackLink columnHeader = new AjaxFallbackLink("th_" + c.propertyName) {
                @Override
                public void onClick(AjaxRequestTarget ajaxRequestTarget) {
                    if (sortingDescription == null || !sortingDescription.field.equals(c.sortColumnName)) {
                        sortingDescription = new SortingDescription(c.sortColumnName, SortingDescription.Sorting.ASC);
                    } else {
                        sortingDescription.sorting = sortingDescription.sorting == SortingDescription.Sorting.ASC ?
                                SortingDescription.Sorting.DESC : SortingDescription.Sorting.ASC;
                    }
                    ajaxRequestTarget.add(AjaxTextTable.this);
                }
            };
            columnHeader.setBody(Model.of(c.name));
            add(columnHeader);
        });
    }

    // Добавляет колонку, генерируя имя столбца БД путем перевода имени поля
    // класса модели из верблюжбей нотации в имя с подчеркиваниями
    public void addColumn(String name, String propertyName) {
        addColumn(name, propertyName, CommonUtils.toUnderline(propertyName, true));
    }

    // Здесь имя столбца БД заполняется руками
    public void addColumn(String name, String propertyName, String sortColumnName) {
        columns.add(new Column(name, propertyName, sortColumnName));
    }

    // Здесь дополнительно указывается конвертер, имя колонки БД генерируется автоматически
    public void addColumn(String name, String propertyName, IConverter converter) {
        columns.add(new Column(name, propertyName, CommonUtils.toUnderline(propertyName, true), converter));
    }

    // Все параметры колонки задаются руками
    public void addColumn(String name, String propertyName, String sortColumnName, IConverter converter) {
        columns.add(new Column(name, propertyName, sortColumnName, converter));
    }

    // Возвращает список объектов, необходжимо учитывать сортировку!
    // Используется при каждой перезагрузке таблицы
    protected abstract List<T> loadData(String sortingField, SortingDescription.Sorting sorting);

    // Вызывается при клике на строку
    protected void onClick(AjaxRequestTarget target, T object) {}

}
