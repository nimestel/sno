package sno.web.component;

import org.apache.wicket.datetime.DateConverter;
import org.apache.wicket.datetime.PatternDateConverter;
import org.apache.wicket.datetime.markup.html.form.DateTextField;
import org.apache.wicket.extensions.yui.calendar.DatePicker;
import org.apache.wicket.model.IModel;

import java.util.Date;

/**
 * Расширение DateTextField, в котором определен формат даты и параметры
 * отображения поля.
 */
public class SimpleDateField extends DateTextField {

    private static final DateConverter DATE_CONVERTER = new PatternDateConverter("dd.MM.yyyy", false);

    public SimpleDateField(String id) {
        super(id, DATE_CONVERTER);
    }

    public SimpleDateField(String id, IModel<Date> model) {
        super(id, model, DATE_CONVERTER);
    }

    @Override
    protected void onInitialize() {
        super.onInitialize();

        DatePicker datePicker = new DatePicker();
        datePicker.setShowOnFieldClick(true);
        datePicker.setAutoHide(true);
        add(datePicker);
    }
}
