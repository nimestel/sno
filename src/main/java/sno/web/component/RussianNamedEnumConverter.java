package sno.web.component;

import org.apache.wicket.util.convert.ConversionException;
import org.apache.wicket.util.convert.IConverter;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.Locale;

/**
 * Конвертер для всех перечислений из DTO.
 * Для всех таких перечислений определен метод getRussianName, его
 * возвращаемое значение и будет отображаться.
 * @param <T> Класс перечисления.
 */
public class RussianNamedEnumConverter<T extends Enum> implements IConverter<T> {

    @Override
    public T convertToObject(String s, Locale locale) throws ConversionException {
        // Не нужно
        return null;
    }

    @Override
    public String convertToString(T t, Locale locale) {
        if (t == null) return "";

        try {
            Method method = t.getClass().getDeclaredMethod("getRussianName");
            return (String) method.invoke(t);
        } catch (NoSuchMethodException | InvocationTargetException | IllegalAccessException e) {
            return t.toString();
        }
    }

}
