package sno.util;

import sno.db.dto.base.BaseDTO;

import java.io.*;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * Вспомогательные методы, которые могут быть использованы в
 * любом месте проекта.
 */
public class CommonUtils {

    /**
     * Переводит название из верблюжьей нотации в нотацию с подчеркиваниями.
     * Например, abc.superField.wow -> abc_super_field_wow
     * @param s Строка в верблюжьей нотации
     * @param deleteDTO Если true, из строки удаляются все входения 'DTO'
     * @return Строка в нотации с подчеркиваниями
     */
    public static String toUnderline(String s, boolean deleteDTO) {
        if (deleteDTO) {
            s = s.replaceAll("DTO", "");
        }

        StringBuilder stringBuilder = new StringBuilder();
        char[] chars = s.toCharArray();
        for (int i = 0; i < chars.length; i++) {
            char ch = chars[i];
            if (Character.isLetter(ch) && Character.isUpperCase(ch)) {
                if (i != 0) stringBuilder.append('_');
                stringBuilder.append(Character.toLowerCase(ch));
            } else if (ch == '.') {
                stringBuilder.append('_');
            } else {
                stringBuilder.append(ch);
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Клонирует полностью обхект в памяти через сериализацию.
     * @param orig Объект для клонирования
     * @param <T> Класс объекта
     * @return Полная копия объекта в памяти (включая внутренние объекты)
     */
    @SuppressWarnings("unchecked")
    public static <T> T cloneObject(T orig) {
        T obj;
        try {
            ByteArrayOutputStream bos = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(bos);
            out.writeObject(orig);
            out.flush();
            out.close();

            ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(bos.toByteArray()));
            obj = (T) in.readObject();
        } catch (IOException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return obj;
    }

    /**
     * Сравнивает два объекта по id.
     * @param obj1 Объект 1
     * @param obj2 Объект 2
     * @param <T> Класс объектов
     * @return true, если id одинаковые, либо отсутсвуют у обоих объектов
     */
    public static <T extends BaseDTO> boolean equalsById(T obj1, T obj2) {
        if (obj1 == null) return obj2 == null;
        if (obj2 == null) return false;
        if (obj1.getId() == null) return obj2.getId() == null;
        return obj1.getId().equals(obj2.getId());
    }

    /**
     * Генерирует MD5 строки
     * @param from Строка
     * @return MD5
     * @throws NoSuchAlgorithmException На самом деле, никогда не будет выброшено
     *  (разве что если в JRE не реализован MD5)
     */
    public static String generateMD5(String from) throws NoSuchAlgorithmException {
        MessageDigest messageDigest;
        byte[] digest;

        messageDigest = MessageDigest.getInstance("MD5");
        messageDigest.reset();
        messageDigest.update(from.getBytes());
        digest = messageDigest.digest();

        BigInteger bigInt = new BigInteger(1, digest);
        String md5Hex = bigInt.toString(16);

        while(md5Hex.length() < 32){
            md5Hex = "0" + md5Hex;
        }

        return md5Hex;
    }

}
