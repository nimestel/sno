package sno.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import sno.db.dao.UniversityStructureDAO;
import sno.db.dto.EventDTO;
import sno.db.dto.StudentDTO;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SelectionUtil {

    @Autowired
    private UniversityStructureDAO universityStructureDAO;

    // TODO Может все это можно сделать быстрее??
    public Map<Long, Double> calculateStudentInterest(StudentDTO studentDTO) {
        List<EventDTO> eventDTOList = universityStructureDAO.getEvents(null);
        List<Long> studentInterests = universityStructureDAO.getInterestingDisciplinesIdsForStudent(studentDTO);
        HashMap<Long, Double> res = new HashMap<>();

        int studentInterestSize = studentInterests.size();
        int intersectionSize;
        for (EventDTO eventDTO : eventDTOList) {
            double k = 0;
            if (studentInterestSize != 0) {
                List<Long> eventDisciplines = universityStructureDAO.getInterestingDisciplinesIdsForEvent(eventDTO);
                intersectionSize = calculateIntersectionSize(studentInterests, eventDisciplines);
                k = 1. * intersectionSize / studentInterestSize;
            }
            res.put(eventDTO.getId(), k);
        }

        return res;
    }

    public Map<Long, Double> calculateEventInterest(EventDTO eventDTO) {
        List<StudentDTO> studentDTOList = universityStructureDAO.getStudents(null);
        List<Long> eventDisciplines = universityStructureDAO.getInterestingDisciplinesIdsForEvent(eventDTO);
        HashMap<Long, Double> res = new HashMap<>();

        int eventDisciplinesSize = eventDisciplines.size();
        int intersectionSize;
        for (StudentDTO studentDTO: studentDTOList) {
            double k = 0;
            if (eventDisciplinesSize != 0) {
                List<Long> studentInterests = universityStructureDAO.getInterestingDisciplinesIdsForStudent(studentDTO);
                intersectionSize = calculateIntersectionSize(eventDisciplines, studentInterests);
                k = 1. * intersectionSize / eventDisciplinesSize;
            }
            res.put(studentDTO.getId(), k);
        }

        return res;
    }

    // TODO Это точно можон сделать быстрее
    private int calculateIntersectionSize(List<Long> l1, List<Long> l2) {
        int res = 0;
        for (Long value : l1) {
            if (l2.contains(value)) res++;
        }
        return res;
    }

}
