package sno.db.dao;

import org.springframework.stereotype.Service;
import sno.db.dto.*;
import sno.db.dto.base.BaseCommentedPersonDTO;
import sno.db.sc.BaseSC;
import sno.db.sc.EventParticipationSC;
import sno.db.util.CascadeRowMapper;

import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service("universityStructureDAO")
public class UniversityStructureDAOImpl extends BaseDAO implements UniversityStructureDAO {

    @Override
    public List<FacultyDTO> getFaculties(BaseSC sc) {
        return get(sc, "faculty_v", facultyDTOCascadeRowMapper);
    }

    @Override
    public List<DepartmentDTO> getDepartments(BaseSC sc) {
        return get(sc, "department_v", departmentDTOCascadeRowMapper);
    }

    @Override
    public List<GroupDTO> getGroups(BaseSC sc) {
        return get(sc, "_group_v", groupDTOCascadeRowMapper);
    }

    @Override
    public List<StudentDTO> getStudents(BaseSC sc) {
        return get(sc, "student_v", studentDTOCascadeRowMapper);
    }

    @Override
    public StudentDTO getStudentById(long id) {
        return jdbcTemplate.queryForObject("select * from student_v where id_student = ?", studentDTOCascadeRowMapper, id);
    }

    @Override
    public long postStudent(StudentDTO studentDTO) {
        String sql;
        if (studentDTO.getId() == null) {
            sql = "insert into student(" +
                    "last_name," +
                    "first_name," +
                    "patronymic," +
                    "gender," +
                    "birth_date," +
                    "id__group," +
                    "begin_date," +
                    "end_date," +
                    "sno_functions," +
                    "hobby," +
                    "commentary" +
                    ") values (?, ?, ?, cast(? as gender_type), ?, ?, ?, ?, ?, ?, ?)";
        } else {
            sql = "update student set " +
                    "last_name = ?," +
                    "first_name = ?," +
                    "patronymic = ?," +
                    "gender = cast(? as gender_type)," +
                    "birth_date = ?," +
                    "id__group = ?," +
                    "begin_date = ?," +
                    "end_date = ?," +
                    "sno_functions = ?," +
                    "hobby = ?," +
                    "commentary = ? " +
                    "where id_student = " + Long.toString(studentDTO.getId());
        }
        return jdbcTemplate.insertReturnId(sql, "id_student",
                studentDTO.getLastName(),
                studentDTO.getFirstName(),
                studentDTO.getPatronymic(),
                Optional.ofNullable(studentDTO.getGender()).map(StudentDTO.Gender::getRussianName).orElse(null),
                studentDTO.getBirthDate(),
                getIdOrNull(studentDTO.getGroupDTO()),
                studentDTO.getBeginDate(),
                studentDTO.getEndDate(),
                studentDTO.getSnoFunctions(),
                studentDTO.getHobby(),
                studentDTO.getCommentary()
        );
    }

    @Override
    public void deleteStudentById(long id) {
        jdbcTemplate.update("delete from student where id_student = ?", id);
    }

    @Override
    public List<EventParticipationDTO> getEventParticipations(EventParticipationSC sc) {
        return get(sc, "event_participation_v", eventParticipationDTOCascadeRowMapper);
    }

    @Override
    public List<EventDTO> getEvents(BaseSC sc) {
        return get(sc, "event_v", eventDTOCascadeRowMapper);
    }

    @Override
    public EventDTO getEventById(long id) {
        return jdbcTemplate.queryForObject("select * from event_v where id_event = ?", eventDTOCascadeRowMapper, id);
    }

    @Override
    public List<DisciplineDTO> getDisciplines() {
        return get(null, "discipline_v", disciplineDTOCascadeRowMapper);
    }

    @Override
    public List<DisciplineDTO> getInterestingDisciplinesForStudent(StudentDTO studentDTO) {
        if (studentDTO.getId() == null) return new ArrayList<>();

        return jdbcTemplate.query(
                "select * from discipline_v where id_discipline in (select id_discipline from student_discipline where id_student = ?)",
                disciplineDTOCascadeRowMapper, studentDTO.getId()
        );
    }

    @Override
    public List<Long> getInterestingDisciplinesIdsForStudent(StudentDTO studentDTO) {
        if (studentDTO.getId() == null) return new ArrayList<>();

        return jdbcTemplate.queryForList(
                "select id_discipline from student_discipline where id_student = ?",
                Long.class, studentDTO.getId()
        );
    }

    @Override
    public void postInterestingDisciplinesForStudent(StudentDTO studentDTO, List<DisciplineDTO> disciplineDTOList) {
        if (studentDTO.getId() == null) return;

        for (DisciplineDTO disciplineDTO : disciplineDTOList) {
            if (disciplineDTO.getId() == null) continue;
            jdbcTemplate.update(
                    "insert into student_discipline(id_student, id_discipline) values(?, ?)",
                    studentDTO.getId(), disciplineDTO.getId()
            );
        }
    }

    @Override
    public void deleteInterestingDisciplinesForStudent(StudentDTO studentDTO, List<DisciplineDTO> disciplineDTOList) {
        if (studentDTO.getId() == null) return;

        for (DisciplineDTO disciplineDTO : disciplineDTOList) {
            if (disciplineDTO.getId() == null) continue;
            jdbcTemplate.update(
                    "delete from student_discipline where id_student = ? and id_discipline = ?",
                    studentDTO.getId(), disciplineDTO.getId()
            );
        }
    }

    @Override
    public List<DisciplineDTO> getInterestingDisciplinesForEvent(EventDTO eventDTO) {
        if (eventDTO.getId() == null) return new ArrayList<>();

        return jdbcTemplate.query(
                "select * from discipline_v where id_discipline in (select id_discipline from event_discipline where id_event = ?)",
                disciplineDTOCascadeRowMapper, eventDTO.getId()
        );
    }

    @Override
    public List<Long> getInterestingDisciplinesIdsForEvent(EventDTO eventDTO) {
        if (eventDTO.getId() == null) return new ArrayList<>();

        return jdbcTemplate.queryForList(
                "select id_discipline from event_discipline where id_event = ?",
                Long.class, eventDTO.getId()
        );
    }

    @Override
    public void postInterestingDisciplinesForEvent(EventDTO eventDTO, List<DisciplineDTO> disciplineDTOList) {
        if (eventDTO.getId() == null) return;

        for (DisciplineDTO disciplineDTO : disciplineDTOList) {
            if (disciplineDTO.getId() == null) continue;
            jdbcTemplate.update(
                    "insert into event_discipline(id_event, id_discipline) values(?, ?)",
                    eventDTO.getId(), disciplineDTO.getId()
            );
        }
    }

    @Override
    public void deleteInterestingDisciplinesForEvent(EventDTO eventDTO, List<DisciplineDTO> disciplineDTOList) {
        if (eventDTO.getId() == null) return;

        for (DisciplineDTO disciplineDTO : disciplineDTOList) {
            if (disciplineDTO.getId() == null) continue;
            jdbcTemplate.update(
                    "delete from event_discipline where id_event = ? and id_discipline = ?",
                    eventDTO.getId(), disciplineDTO.getId()
            );
        }
    }

    // ------------------ RowMapper'ы

    private CascadeRowMapper<FacultyDTO> facultyDTOCascadeRowMapper =
            new BaseNamedCommentedDTORowMapper<FacultyDTO>(FacultyDTO.class)
    {
        @Override
        protected void fillFieldMappers() {
            super.fillFieldMappers();

            pfm("id_faculty", ResultSet::getLong, FacultyDTO::setId);
            pfm("dean", ResultSet::getString, FacultyDTO::setDean);
        }
    };

    private CascadeRowMapper<DepartmentDTO> departmentDTOCascadeRowMapper =
            new BaseNamedCommentedDTORowMapper<DepartmentDTO>(DepartmentDTO.class)
    {
        @Override
        protected void fillFieldMappers() {
            super.fillFieldMappers();

            pfm("id_department", ResultSet::getLong, DepartmentDTO::setId);
            pfm("head_of_a_chair", ResultSet::getString, DepartmentDTO::setHeadOfAChair);

            dfm("faculty_", facultyDTOCascadeRowMapper, DepartmentDTO::setFacultyDTO);
        }
    };

    private CascadeRowMapper<GroupDTO> groupDTOCascadeRowMapper =
            new BaseNamedCommentedDTORowMapper<GroupDTO>(GroupDTO.class)
    {
        @Override
        protected void fillFieldMappers() {
            super.fillFieldMappers();

            pfm("id__group", ResultSet::getLong, GroupDTO::setId);
            pfm("year", ResultSet::getInt, GroupDTO::setYear);

            dfm("department_", departmentDTOCascadeRowMapper, GroupDTO::setDepartmentDTO);
        }
    };

    private CascadeRowMapper<StudentDTO> studentDTOCascadeRowMapper = new CascadeRowMapper<StudentDTO>(StudentDTO.class) {
        @Override
        protected void fillFieldMappers() {
            pfm("id_student", ResultSet::getLong, StudentDTO::setId);
            pfm("last_name", ResultSet::getString, StudentDTO::setLastName);
            pfm("first_name", ResultSet::getString, StudentDTO::setFirstName);
            pfm("patronymic", ResultSet::getString, StudentDTO::setPatronymic);
            pfm("gender", ResultSet::getString, (dto, s) -> dto.setGender(StudentDTO.Gender.fromString(s)));
            pfm("birth_date", ResultSet::getTimestamp, (dto, ts) -> dto.setBirthDate(timestampToDate(ts)));
            pfm("begin_date", ResultSet::getTimestamp, (dto, ts) -> dto.setBeginDate(timestampToDate(ts)));
            pfm("end_date", ResultSet::getTimestamp, (dto, ts) -> dto.setEndDate(timestampToDate(ts)));
            pfm("sno_functions", ResultSet::getString, StudentDTO::setSnoFunctions);
            pfm("hobby", ResultSet::getString, StudentDTO::setHobby);
            pfm("commentary", ResultSet::getString, StudentDTO::setCommentary);

            dfm("_group_", groupDTOCascadeRowMapper, StudentDTO::setGroupDTO);
        }
    };

    private CascadeRowMapper<PrikazDTO> prikazDTOCascadeRowMapper = new CascadeRowMapper<PrikazDTO>(PrikazDTO.class) {
        @Override
        protected void fillFieldMappers() {
            pfm("id_prikaz", ResultSet::getLong, PrikazDTO::setId);
            pfm("number_prikaz", ResultSet::getLong, PrikazDTO::setNumberPrikaz);
            pfm("date_prikaz", ResultSet::getTimestamp, (dto, ts) -> dto.setDatePrikaz(timestampToDate(ts)));
            pfm("text_prikaz", ResultSet::getString, PrikazDTO::setTextPrikaz);
        }
    };

    private CascadeRowMapper<OrganizationDTO> organizationDTOCascadeRowMapper =
            new CascadeRowMapper<OrganizationDTO>(OrganizationDTO.class)
    {
        @Override
        protected void fillFieldMappers() {
            pfm("id_organization", ResultSet::getLong, OrganizationDTO::setId);
            pfm("name", ResultSet::getString, OrganizationDTO::setName);
            pfm("city", ResultSet::getString, OrganizationDTO::setCity);
            pfm("organization_level", ResultSet::getString,
                    (dto, s) -> dto.setOrganizationLevel(OrganizationDTO.OrganizationLevel.fromString(s)));
            pfm("organization_type", ResultSet::getString,
                    (dto, s) -> dto.setOrganizationType(OrganizationDTO.OrganizationType.fromString(s)));
            pfm("field", ResultSet::getString, OrganizationDTO::setField);
            pfm("is_affiliated", ResultSet::getString,
                    (dto, s) -> dto.setIsAffiliated(OrganizationDTO.AffiliationType.fromString(s)));
            pfm("commentary", ResultSet::getString, OrganizationDTO::setCommentary);
        }
    };

    private CascadeRowMapper<EmployeeDTO> employeeDTOCascadeRowMapper =
            new BaseCommentedPersonDTORowMapper<EmployeeDTO>(EmployeeDTO.class)
    {
        @Override
        protected void fillFieldMappers() {
            super.fillFieldMappers();

            pfm("id_employee", ResultSet::getLong, EmployeeDTO::setId);
            pfm("job", ResultSet::getString, EmployeeDTO::setJob);
            pfm("academic_status", ResultSet::getString, EmployeeDTO::setAcademicStatus);
            pfm("academic_degree", ResultSet::getString, EmployeeDTO::setAcademicDegree);

            dfm("department_", departmentDTOCascadeRowMapper, EmployeeDTO::setDepartmentDTO);
        }
    };

    private CascadeRowMapper<ContactPersonDTO> contactPersonDTOCascadeRowMapper =
            new BaseCommentedPersonDTORowMapper<ContactPersonDTO>(ContactPersonDTO.class)
    {
        @Override
        protected void fillFieldMappers() {
            super.fillFieldMappers();

            pfm("id_contact_person", ResultSet::getLong, ContactPersonDTO::setId);
            pfm("gender", ResultSet::getString, (dto, s) -> dto.setGender(BaseCommentedPersonDTO.Gender.fromString(s)));
            pfm("job", ResultSet::getString, ContactPersonDTO::setJob);

            dfm("organization_", organizationDTOCascadeRowMapper, ContactPersonDTO::setOrganizationDTO);
        }
    };

    private CascadeRowMapper<EventDTO> eventDTOCascadeRowMapper = new CascadeRowMapper<EventDTO>(EventDTO.class) {
        @Override
        protected void fillFieldMappers() {
            pfm("id_event", ResultSet::getLong, EventDTO::setId);
            pfm("event_type", ResultSet::getString, (dto, s) -> dto.setEventType(EventDTO.EventType.fromString(s)));
            pfm("name", ResultSet::getString, EventDTO::setName);
            pfm("begin_date", ResultSet::getTimestamp, (dto, ts) -> dto.setBeginDate(timestampToDate(ts)));
            pfm("end_date", ResultSet::getTimestamp, (dto, ts) -> dto.setEndDate(timestampToDate(ts)));
            pfm("registration_begin_date", ResultSet::getTimestamp,
                    (dto, ts) -> dto.setRegistrationBeginDate(timestampToDate(ts)));
            pfm("registration_end_date", ResultSet::getTimestamp,
                    (dto, ts) -> dto.setRegistrationEndDate(timestampToDate(ts)));
            pfm("status", ResultSet::getString, (dto, s) -> dto.setStatus(EventDTO.StatusType.fromString(s)));
            pfm("prize", ResultSet::getDouble, EventDTO::setPrize);
            pfm("commentary", ResultSet::getString, EventDTO::setCommentary);
            pfm("link", ResultSet::getString, EventDTO::setLink);
            pfm("file", ResultSet::getString, EventDTO::setFile);

            dfm("cp_", contactPersonDTOCascadeRowMapper, EventDTO::setContactPersonDTO);
            dfm("employee_", employeeDTOCascadeRowMapper, EventDTO::setEmployeeDTO);
            dfm("ppr_", prikazDTOCascadeRowMapper, EventDTO::setPrikazProved);
            dfm("ppo_", prikazDTOCascadeRowMapper, EventDTO::setPrikazPoochr);
        }
    };

    // TODO Эта штука выглядит некрасиво
    private CascadeRowMapper<EventParticipationDTO> eventParticipationDTOCascadeRowMapper =
            new CascadeRowMapper<EventParticipationDTO>(EventParticipationDTO.class)
    {
        @Override
        protected void fillFieldMappers() {
            pfm("begin_date", ResultSet::getTimestamp,(dto, ts) -> dto.setBeginDate(timestampToDate(ts)));
            pfm("end_date", ResultSet::getTimestamp,(dto, ts) -> dto.setEndDate(timestampToDate(ts)));
            pfm("place", ResultSet::getLong, EventParticipationDTO::setPlace);
            pfm("prize", ResultSet::getDouble, EventParticipationDTO::setPrize);
            pfm("commentary", ResultSet::getString, EventParticipationDTO::setCommentary);
            pfm("link", ResultSet::getString, EventParticipationDTO::setLink);
            pfm("file", ResultSet::getString, EventParticipationDTO::setFile);

            dfm("student_", new CascadeRowMapper<StudentDTO>(StudentDTO.class) {
                @Override
                protected void fillFieldMappers() {
                    pfm("id_student", ResultSet::getLong, StudentDTO::setId);
                    pfm("last_name", ResultSet::getString, StudentDTO::setLastName);
                    pfm("first_name", ResultSet::getString, StudentDTO::setFirstName);

                    dfm("_group_", new CascadeRowMapper<GroupDTO>(GroupDTO.class) {
                        @Override
                        protected void fillFieldMappers() {
                            pfm("acronym", ResultSet::getString, GroupDTO::setAcronym);
                            pfm("year", ResultSet::getInt, GroupDTO::setYear);
                        }
                    }, StudentDTO::setGroupDTO);
                }
            }, EventParticipationDTO::setStudentDTO);

            dfm("event_", new CascadeRowMapper<EventDTO>(EventDTO.class) {
                @Override
                protected void fillFieldMappers() {
                    pfm("id_event", ResultSet::getLong, EventDTO::setId);
                    pfm("event_type", ResultSet::getString, (dto, s) -> dto.setEventType(EventDTO.EventType.fromString(s)));
                    pfm("name", ResultSet::getString, EventDTO::setName);
                    pfm("status", ResultSet::getString, (dto, s) -> dto.setStatus(EventDTO.StatusType.fromString(s)));
                }
            }, EventParticipationDTO::setEventDTO);
        }
    };

    private CascadeRowMapper<DisciplineDTO> disciplineDTOCascadeRowMapper = new CascadeRowMapper<DisciplineDTO>(DisciplineDTO.class) {
        @Override
        protected void fillFieldMappers() {
            pfm("id_discipline", ResultSet::getLong, DisciplineDTO::setId);
            pfm("name", ResultSet::getString, DisciplineDTO::setName);
        }
    };

}
