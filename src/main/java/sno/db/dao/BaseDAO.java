package sno.db.dao;

import org.springframework.jdbc.core.RowMapper;
import sno.db.dto.EmployeeDTO;
import sno.db.dto.base.BaseCommentedPersonDTO;
import sno.db.dto.base.BaseDTO;
import sno.db.dto.base.BaseNamedCommentedDTO;
import sno.db.sc.BaseSC;
import sno.db.util.CascadeRowMapper;
import sno.db.util.QueryBuilder;
import sno.db.util.SNOJdbcTemplate;

import javax.annotation.Resource;
import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

/**
 * Все DAO наследуются от BaseDAO.
 * Здесь заполяется SNOJdbcTemplate, определены вспомогательные методы,
 * при необходимости определяются общие CascadeRowMapper'ы.
 */
abstract class BaseDAO {

    protected SNOJdbcTemplate jdbcTemplate;

    @Resource
    @SuppressWarnings("unused")
    public final void setDataSource(DataSource dataSource) {
        this.jdbcTemplate = new SNOJdbcTemplate(dataSource);
    }

    protected <T extends BaseDTO, SC extends BaseSC, RM extends RowMapper<T>> List<T> get(SC sc, String viewName, RM rowMapper) {
        QueryBuilder builder = new QueryBuilder("select * from " + viewName);
        if (sc != null) sc.fill(builder);
        return jdbcTemplate.query(builder.getQuery(), rowMapper, builder.getArguments());
    }

    protected static Date timestampToDate(Timestamp timestamp) {
        if (timestamp == null) return null;
        return new Date(timestamp.getTime());
    }

    protected <T extends BaseDTO> Long getIdOrNull(T obj) {
        return Optional.ofNullable(obj).map(BaseDTO::getId).orElse(null);
    }

    protected class BaseNamedCommentedDTORowMapper<T extends BaseNamedCommentedDTO> extends CascadeRowMapper<T> {
        public BaseNamedCommentedDTORowMapper(Class<T> dtoClass) {
            super(dtoClass);
        }

        @Override
        protected void fillFieldMappers() {
            pfm("name", ResultSet::getString, T::setName);
            pfm("acronym", ResultSet::getString, T::setAcronym);
            pfm("commentary", ResultSet::getString, T::setCommentary);
        }
    }

    protected class BaseCommentedPersonDTORowMapper<T extends BaseCommentedPersonDTO> extends CascadeRowMapper<T> {
        public BaseCommentedPersonDTORowMapper(Class<T> dtoClass) {
            super(dtoClass);
        }

        @Override
        protected void fillFieldMappers() {
            pfm("last_name", ResultSet::getString, T::setLastName);
            pfm("first_name", ResultSet::getString, T::setFirstName);
            pfm("patronymic", ResultSet::getString, T::setPatronymic);
            pfm("commentary", ResultSet::getString, T::setCommentary);
        }
    }

}
