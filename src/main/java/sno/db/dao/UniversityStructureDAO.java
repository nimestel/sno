package sno.db.dao;

import sno.db.dto.*;
import sno.db.sc.BaseSC;
import sno.db.sc.EventParticipationSC;

import java.util.List;

public interface UniversityStructureDAO {

    List<FacultyDTO> getFaculties(BaseSC sc);

    List<DepartmentDTO> getDepartments(BaseSC sc);

    List<GroupDTO> getGroups(BaseSC sc);

    List<StudentDTO> getStudents(BaseSC sc);

    StudentDTO getStudentById(long id);

    long postStudent(StudentDTO studentDTO);

    void deleteStudentById(long id);

    List<EventParticipationDTO> getEventParticipations(EventParticipationSC sc);

    List<EventDTO> getEvents(BaseSC sc);

    EventDTO getEventById(long id);

    List<DisciplineDTO> getDisciplines();

    List<DisciplineDTO> getInterestingDisciplinesForStudent(StudentDTO studentDTO);

    List<Long> getInterestingDisciplinesIdsForStudent(StudentDTO studentDTO);

    void postInterestingDisciplinesForStudent(StudentDTO studentDTO, List<DisciplineDTO> disciplineDTOList);

    void deleteInterestingDisciplinesForStudent(StudentDTO studentDTO, List<DisciplineDTO> disciplineDTOList);

    List<DisciplineDTO> getInterestingDisciplinesForEvent(EventDTO eventDTO);

    List<Long> getInterestingDisciplinesIdsForEvent(EventDTO eventDTO);

    void postInterestingDisciplinesForEvent(EventDTO eventDTO, List<DisciplineDTO> disciplineDTOList);

    void deleteInterestingDisciplinesForEvent(EventDTO eventDTO, List<DisciplineDTO> disciplineDTOList);

}
