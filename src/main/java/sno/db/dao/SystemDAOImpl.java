package sno.db.dao;

import org.springframework.stereotype.Service;
import sno.db.dto.AccountDTO;
import sno.db.dto.GroupDTO;
import sno.db.dto.StudentDTO;
import sno.db.util.CascadeRowMapper;

import java.sql.ResultSet;

@Service("systemDAO")
public class SystemDAOImpl  extends BaseDAO implements SystemDAO {

    /**
     * Заглушка без использования БД
     * @param username Имя пользователя
     * @param passwordHash Хэш пароля
     * @return true, если такой пользователь есть
     */
    @Override
    public boolean authenticate(String username, String passwordHash) {
        return jdbcTemplate.queryForObject(
                "select count(id_account) from account_v where login = ? and password_hash = ?", Long.class,
                username, passwordHash
        ) > 0L;
    }

    @Override
    public AccountDTO getAccountByUsername(String username) {
        return jdbcTemplate.queryForObject(
                "select * from account_v where login = ?", accountDTOCascadeRowMapper, username
        );
    }

    private CascadeRowMapper<AccountDTO> accountDTOCascadeRowMapper = new CascadeRowMapper<AccountDTO>(AccountDTO.class) {
        @Override
        protected void fillFieldMappers() {
            pfm("id_account", ResultSet::getLong, AccountDTO::setId);
            pfm("login", ResultSet::getString, AccountDTO::setLogin);
            pfm("password_hash", ResultSet::getString, AccountDTO::setPasswordHash);
            pfm("access_level", ResultSet::getString,
                    (dto, s) -> dto.setAccessLevel(AccountDTO.AccessLevel.fromString(s)));
            pfm("email", ResultSet::getString, AccountDTO::setEmail);
            pfm("info", ResultSet::getString, AccountDTO::setInfo);

            dfm("student_", new CascadeRowMapper<StudentDTO>(StudentDTO.class) {
                @Override
                protected void fillFieldMappers() {
                    pfm("id_student", ResultSet::getLong, StudentDTO::setId);
                    pfm("last_name", ResultSet::getString, StudentDTO::setLastName);
                    pfm("first_name", ResultSet::getString, StudentDTO::setFirstName);
                    pfm("patronymic", ResultSet::getString, StudentDTO::setPatronymic);

                    dfm("_group_", new CascadeRowMapper<GroupDTO>(GroupDTO.class) {
                        @Override
                        protected void fillFieldMappers() {
                            pfm("acronym", ResultSet::getString, GroupDTO::setAcronym);
                            pfm("year", ResultSet::getInt, GroupDTO::setYear);
                        }
                    }, StudentDTO::setGroupDTO);
                }
            }, AccountDTO::setStudentDTO);
        }
    };

}
