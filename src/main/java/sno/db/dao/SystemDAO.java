package sno.db.dao;

import sno.db.dto.AccountDTO;

public interface SystemDAO {

    boolean authenticate(String username, String passwordHash);

    AccountDTO getAccountByUsername(String username);

}