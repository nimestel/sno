package sno.db.sc;

import java.io.Serializable;

/**
 * Описывает ASC/DESC сортировку по столбцу field
 */
public class SortingDescription implements Serializable {

    public enum Sorting {
        ASC, DESC
    }

    public String field;
    public Sorting sorting;

    public SortingDescription(String field, Sorting sorting) {
        this.field = field;
        this.sorting = sorting;
    }

}
