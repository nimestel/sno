package sno.db.sc;

import sno.db.util.QueryBuilder;

public class EventParticipationSC extends BaseSC {

    private Long studentId;
    private Long eventId;

    @Override
    public void fill(QueryBuilder queryBuilder) {
        super.fill(queryBuilder);

        queryBuilder.eq("event_id_event", eventId);
        queryBuilder.eq("student_id_student", studentId);
    }

    public Long getStudentId() {
        return studentId;
    }

    public void setStudentId(Long studentId) {
        this.studentId = studentId;
    }

    public Long getEventId() {
        return eventId;
    }

    public void setEventId(Long eventId) {
        this.eventId = eventId;
    }
}
