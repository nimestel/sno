package sno.db.util;

import org.springframework.jdbc.core.ArgumentPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementSetter;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import javax.sql.DataSource;
import java.sql.PreparedStatement;

public class SNOJdbcTemplate extends JdbcTemplate {

    public SNOJdbcTemplate(DataSource dataSource) {
        super(dataSource);
    }

    /**
     * Добавление новой строки в таблицу БД с возвращением id добавленной строки
     * @param sql SQL для добавления
     * @param idColumnName Имя стоблца с id
     * @param args Аргументы запроса
     * @return id вставленной строки
     */
    public Long insertReturnId(String sql, String idColumnName, Object... args) {
        PreparedStatementSetter preparedStatementSetter = new ArgumentPreparedStatementSetter(args);
        KeyHolder keyHolder = new GeneratedKeyHolder();

        update(connection -> {
            PreparedStatement ps = connection.prepareStatement(sql, new String[]{ idColumnName });
            preparedStatementSetter.setValues(ps);
            return ps;
        }, keyHolder);

        return keyHolder.getKey().longValue();
    }

}
