package sno.db.util;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.BiConsumer;

/**
 * Упрощает создание RowMapper'ов для составных DTO (одно DTO в составе другого)
 * @param <T> Класс DTO
 */
public abstract class CascadeRowMapper<T> implements RowMapper<T> {

    // Префикс используется при маппинге полей внутренних DTO.
    // Имена столбков во вьюхах сделаны по принципу:
    // если таблица tableA имеет внешний ключ на таблицу tableB, то её
    // поле tableB.col будет названо во вьюхе tableB_col
    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    @FunctionalInterface
    public interface SQLExceptionBiFunction<T1, T2, R> {
        R apply(T1 arg1, T2 arg2) throws SQLException;
    }

    /**
     * Маппер абстрактного поля DTO
     */
    private abstract class FieldMapper {
        protected String columnName;

        protected FieldMapper(String columnName) {
            this.columnName = columnName;
        }

        public abstract void map(ResultSet resultSet, T dto, String prefix) throws SQLException;
    }

    /**
     * Маппер примитивного поля DTO, которое не является другим DTO
     * @param <F> Класс поля
     */
    private class PrimitiveFieldMapper<F> extends FieldMapper {
        private SQLExceptionBiFunction<ResultSet, String, F> resultSetGetter;
        private BiConsumer<T, F> dtoSetter;

        public PrimitiveFieldMapper(String columnName, SQLExceptionBiFunction<ResultSet, String, F> resultSetGetter, BiConsumer<T, F> dtoSetter) {
            super(columnName);

            this.resultSetGetter = resultSetGetter;
            this.dtoSetter = dtoSetter;
        }

        @Override
        public void map(ResultSet resultSet, T dto, String prefix) throws SQLException {
            F value = resultSetGetter.apply(resultSet, prefix + columnName);
            dtoSetter.accept(dto, value);
        }
    }

    /**
     * Маппер составного поля DTO, которое является другим DTO
     * @param <D> Класс поля
     */
    private class DtoFieldMapper<D> extends FieldMapper {
        private CascadeRowMapper<D> rowMapper;
        private BiConsumer<T, D> dtoSetter;

        public DtoFieldMapper(String columnPrefix, CascadeRowMapper<D> rowMapper, BiConsumer<T, D> dtoSetter) {
            super(columnPrefix);

            this.rowMapper = rowMapper;
            this.dtoSetter = dtoSetter;
        }

        @Override
        public void map(ResultSet resultSet, T dto, String prefix) throws SQLException {
            String prevPrefix = rowMapper.getPrefix();
            rowMapper.setPrefix(prefix + columnName);

            D value = rowMapper.mapRow(resultSet, 0);
            dtoSetter.accept(dto, value);

            rowMapper.setPrefix(prevPrefix);
        }
    }

    // Для класса T указываются мапперы всех нужных полей и последовательно
    // вызываются при маппинге resultSet
    private Class<T> dtoClass;
    private List<FieldMapper> fieldMappers;
    private String prefix = "";

    public CascadeRowMapper (Class<T> dtoClass) {
        this.dtoClass = dtoClass;

        fieldMappers = new ArrayList<>();
        fillFieldMappers();
    }

    @Override
    public final T mapRow(ResultSet resultSet, int i) throws SQLException {
        try {
            T dto = dtoClass.newInstance();

            for (FieldMapper fieldMapper : fieldMappers) {
                fieldMapper.map(resultSet, dto, prefix);
            }

            return dto;
        } catch (InstantiationException | IllegalAccessException e) {
            throw new SQLException("Не удалось создать DTO");
        }
    }

    /**
     * Добавляет маппер примитивного поля класса F
     * @param columnName Имя колонки БД
     * @param resultSetGetter Геттер для объекта класса F из resultSet
     * @param dtoSetter Сеттер поля в DTO
     * @param <F> Класс поля
     */
    protected final <F> void pfm(String columnName, SQLExceptionBiFunction<ResultSet, String, F> resultSetGetter, BiConsumer<T, F> dtoSetter) {
        fieldMappers.add(new PrimitiveFieldMapper<>(columnName, resultSetGetter, dtoSetter));
    }

    /**
     * Добавляет маппер составного поля класса D
     * @param columnPrefix Префикс колонок в БД
     * @param rowMapper Маппер для DTO класса D
     * @param dtoSetter Сеттер поля в родительском DTO
     * @param <D> Класс поля
     */
    protected final <D> void dfm(String columnPrefix, CascadeRowMapper<D> rowMapper, BiConsumer<T, D> dtoSetter) {
        fieldMappers.add(new DtoFieldMapper<>(columnPrefix, rowMapper, dtoSetter));
    }

    /**
     * Здесь должны быть добавлены все нужные мапперы полей через pfm и dfm
     */
    protected abstract void fillFieldMappers();

}
