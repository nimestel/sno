package sno.db.dto;

import sno.db.dto.base.BaseNamedCommentedDTO;
import sno.util.CommonUtils;

public class GroupDTO extends BaseNamedCommentedDTO {

    private Integer year;
    private DepartmentDTO departmentDTO;

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public DepartmentDTO getDepartmentDTO() {
        return departmentDTO;
    }

    public void setDepartmentDTO(DepartmentDTO departmentDTO) {
        this.departmentDTO = departmentDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        GroupDTO that = (GroupDTO) o;

        if (year != null ? !year.equals(that.year) : that.year != null) return false;
        return CommonUtils.equalsById(departmentDTO, that.departmentDTO);

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + (departmentDTO != null ? departmentDTO.hashCode() : 0);
        return result;
    }
}
