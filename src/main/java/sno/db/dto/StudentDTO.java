package sno.db.dto;

import sno.db.dto.base.BaseCommentedPersonDTO;

import java.util.Date;

public class StudentDTO extends BaseCommentedPersonDTO {

    public String getSnoFunctions() {
        return snoFunctions;
    }

    public void setSnoFunctions(String snoFunctions) {
        this.snoFunctions = snoFunctions;
    }

    private Gender gender;
    private Date birthDate;
    private Date beginDate;
    private Date endDate;
    private String snoFunctions;
    private String hobby;
    private GroupDTO groupDTO;

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public Date getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(Date birthDate) {
        this.birthDate = birthDate;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public String getHobby() {
        return hobby;
    }

    public void setHobby(String hobby) {
        this.hobby = hobby;
    }

    public GroupDTO getGroupDTO() {
        return groupDTO;
    }

    public void setGroupDTO(GroupDTO groupDTO) {
        this.groupDTO = groupDTO;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        StudentDTO that = (StudentDTO) o;

        if (gender != that.gender) return false;
        if (birthDate != null ? !birthDate.equals(that.birthDate) : that.birthDate != null) return false;
        if (beginDate != null ? !beginDate.equals(that.beginDate) : that.beginDate != null) return false;
        if (endDate != null ? !endDate.equals(that.endDate) : that.endDate != null) return false;
        if (snoFunctions != null ? !snoFunctions.equals(that.snoFunctions) : that.snoFunctions != null) return false;
        if (hobby != null ? !hobby.equals(that.hobby) : that.hobby != null) return false;
        return groupDTO != null ? groupDTO.equals(that.groupDTO) : that.groupDTO == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (gender != null ? gender.hashCode() : 0);
        result = 31 * result + (birthDate != null ? birthDate.hashCode() : 0);
        result = 31 * result + (beginDate != null ? beginDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (snoFunctions != null ? snoFunctions.hashCode() : 0);
        result = 31 * result + (hobby != null ? hobby.hashCode() : 0);
        result = 31 * result + (groupDTO != null ? groupDTO.hashCode() : 0);
        return result;
    }
}
