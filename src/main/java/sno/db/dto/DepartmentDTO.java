package sno.db.dto;

import sno.db.dto.base.BaseNamedCommentedDTO;
import sno.util.CommonUtils;

public class DepartmentDTO extends BaseNamedCommentedDTO {

    private FacultyDTO facultyDTO;
    private String headOfAChair;

    public FacultyDTO getFacultyDTO() {
        return facultyDTO;
    }

    public void setFacultyDTO(FacultyDTO facultyDTO) {
        this.facultyDTO = facultyDTO;
    }

    public String getHeadOfAChair() {
        return headOfAChair;
    }

    public void setHeadOfAChair(String headOfAChair) {
        this.headOfAChair = headOfAChair;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        DepartmentDTO that = (DepartmentDTO) o;

        if (!CommonUtils.equalsById(facultyDTO, that.facultyDTO)) return false;
        return headOfAChair != null ? headOfAChair.equals(that.headOfAChair) : that.headOfAChair == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (facultyDTO != null ? facultyDTO.hashCode() : 0);
        result = 31 * result + (headOfAChair != null ? headOfAChair.hashCode() : 0);
        return result;
    }

}
