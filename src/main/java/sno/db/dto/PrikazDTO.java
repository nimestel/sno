package sno.db.dto;

import sno.db.dto.base.BaseDTO;

import java.util.Date;

public class PrikazDTO extends BaseDTO {

    private Long numberPrikaz;
    private Date datePrikaz;
    private String textPrikaz;

    public Long getNumberPrikaz() {
        return numberPrikaz;
    }

    public void setNumberPrikaz(Long numberPrikaz) {
        this.numberPrikaz = numberPrikaz;
    }

    public Date getDatePrikaz() {
        return datePrikaz;
    }

    public void setDatePrikaz(Date datePrikaz) {
        this.datePrikaz = datePrikaz;
    }

    public String getTextPrikaz() {
        return textPrikaz;
    }

    public void setTextPrikaz(String textPrikaz) {
        this.textPrikaz = textPrikaz;
    }
}
