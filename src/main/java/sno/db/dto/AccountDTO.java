package sno.db.dto;

import sno.db.dto.base.BaseDTO;

public class AccountDTO extends BaseDTO {

    public AccessLevel getAccessLevel() {
        return accessLevel;
    }

    public void setAccessLevel(AccessLevel accessLevel) {
        this.accessLevel = accessLevel;
    }

    public enum AccessLevel {
        ADMIN("Администратор"), STUDENT("Студент");

        private String russianName;

        AccessLevel(String russianName) {
            this.russianName = russianName;
        }

        public static AccessLevel fromString(String s) {
            if (s == null) return null;

            switch (s) {
                case "Администратор":
                    return ADMIN;
                case "Студент":
                    return STUDENT;
                default:
                    return null;
            }
        }

        public String getRussianName() {
            return russianName;
        }
    }

    private String login;
    private String passwordHash;
    private AccessLevel accessLevel;
    private String email;
    private String info;
    private StudentDTO studentDTO;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPasswordHash() {
        return passwordHash;
    }

    public void setPasswordHash(String passwordHash) {
        this.passwordHash = passwordHash;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getInfo() {
        return info;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public StudentDTO getStudentDTO() {
        return studentDTO;
    }

    public void setStudentDTO(StudentDTO studentDTO) {
        this.studentDTO = studentDTO;
    }
}
