package sno.db.dto;

import sno.db.dto.base.BaseCommentedPersonDTO;

public class ContactPersonDTO extends BaseCommentedPersonDTO {

    private Gender gender;
    private OrganizationDTO organizationDTO;
    private String job;

    public Gender getGender() {
        return gender;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

    public OrganizationDTO getOrganizationDTO() {
        return organizationDTO;
    }

    public void setOrganizationDTO(OrganizationDTO organizationDTO) {
        this.organizationDTO = organizationDTO;
    }

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }
}
