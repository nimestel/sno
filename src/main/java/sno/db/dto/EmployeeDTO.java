package sno.db.dto;

import sno.db.dto.base.BaseCommentedPersonDTO;

public class EmployeeDTO extends BaseCommentedPersonDTO {

    private String job;
    private DepartmentDTO departmentDTO;
    private String academicStatus;
    private String academicDegree;

    public String getJob() {
        return job;
    }

    public void setJob(String job) {
        this.job = job;
    }

    public DepartmentDTO getDepartmentDTO() {
        return departmentDTO;
    }

    public void setDepartmentDTO(DepartmentDTO departmentDTO) {
        this.departmentDTO = departmentDTO;
    }

    public String getAcademicStatus() {
        return academicStatus;
    }

    public void setAcademicStatus(String academicStatus) {
        this.academicStatus = academicStatus;
    }

    public String getAcademicDegree() {
        return academicDegree;
    }

    public void setAcademicDegree(String academicDegree) {
        this.academicDegree = academicDegree;
    }
}
