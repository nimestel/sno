package sno.db.dto.base;

public class BaseCommentedPersonDTO extends BaseCommentedDTO {

    public enum Gender {
        M("Мужской"), F("Женский");

        private String russianName;

        Gender(String russianName) {
            this.russianName = russianName;
        }

        public static Gender fromString(String s) {
            if (s == null) return null;

            switch (s) {
                case "Мужской":
                    return M;
                case "Женский":
                    return F;
                default:
                    return null;
            }
        }

        public String getRussianName() {
            return russianName;
        }
    }

    private String lastName;
    private String firstName;
    private String patronymic;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BaseCommentedPersonDTO that = (BaseCommentedPersonDTO) o;

        if (lastName != null ? !lastName.equals(that.lastName) : that.lastName != null) return false;
        if (firstName != null ? !firstName.equals(that.firstName) : that.firstName != null) return false;
        return patronymic != null ? patronymic.equals(that.patronymic) : that.patronymic == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (lastName != null ? lastName.hashCode() : 0);
        result = 31 * result + (firstName != null ? firstName.hashCode() : 0);
        result = 31 * result + (patronymic != null ? patronymic.hashCode() : 0);
        return result;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getPatronymic() {
        return patronymic;
    }

    public void setPatronymic(String patronymic) {
        this.patronymic = patronymic;
    }

}
