package sno.db.dto.base;

public class BaseNamedCommentedDTO extends BaseCommentedDTO {

    private String name;
    private String acronym;

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BaseNamedCommentedDTO that = (BaseNamedCommentedDTO) o;

        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return acronym != null ? acronym.equals(that.acronym) : that.acronym == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (acronym != null ? acronym.hashCode() : 0);
        return result;
    }
}
