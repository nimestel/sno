package sno.db.dto.base;

public class BaseCommentedDTO extends BaseDTO {

    private String commentary;

    public String getCommentary() {
        return commentary;
    }

    public void setCommentary(String commentary) {
        this.commentary = commentary;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        BaseCommentedDTO that = (BaseCommentedDTO) o;

        return commentary != null ? commentary.equals(that.commentary) : that.commentary == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (commentary != null ? commentary.hashCode() : 0);
        return result;
    }
}
