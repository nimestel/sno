package sno.db.dto;

import sno.db.dto.base.BaseCommentedDTO;

public class OrganizationDTO extends BaseCommentedDTO {

    public enum OrganizationType {
        STATE("Государственная"), SOCIAL("Общественная");

        private String russianName;

        OrganizationType(String russianName) {
            this.russianName = russianName;
        }

        public static OrganizationType fromString(String s) {
            if (s == null) return null;

            switch (s) {
                case "Государственная":
                    return STATE;
                case "Общественная":
                    return SOCIAL;
                default:
                    return null;
            }
        }

        public String getRussianName() {
            return russianName;
        }
    }

    public enum OrganizationLevel {
        FEDERAL("Федеральный"), REGIONAL("Региональный"), MUNICIPAL("Муниципальный");

        private String russianName;

        OrganizationLevel(String russianName) {
            this.russianName = russianName;
        }

        public static OrganizationLevel fromString(String s) {
            if (s == null) return null;

            switch (s) {
                case "Федеральный":
                    return FEDERAL;
                case "Региональный":
                    return REGIONAL;
                case "Муниципальный":
                    return MUNICIPAL;
                default:
                    return null;
            }
        }

        public String getRussianName() {
            return russianName;
        }
    }

    public enum AffiliationType {
        OUR("Наша"), NON_OUR("Не наша");

        private String russianName;

        AffiliationType(String russianName) {
            this.russianName = russianName;
        }

        public static AffiliationType fromString(String s) {
            if (s == null) return null;

            switch (s) {
                case "Наша":
                    return OUR;
                case "Не наша":
                    return NON_OUR;
                default:
                    return null;
            }
        }

        public String getRussianName() {
            return russianName;
        }
    }

    private String name;
    private String city;
    private OrganizationLevel organizationLevel;
    private OrganizationType organizationType;
    private String field;
    private AffiliationType isAffiliated;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public OrganizationLevel getOrganizationLevel() {
        return organizationLevel;
    }

    public void setOrganizationLevel(OrganizationLevel organizationLevel) {
        this.organizationLevel = organizationLevel;
    }

    public OrganizationType getOrganizationType() {
        return organizationType;
    }

    public void setOrganizationType(OrganizationType organizationType) {
        this.organizationType = organizationType;
    }

    public String getField() {
        return field;
    }

    public void setField(String field) {
        this.field = field;
    }

    public AffiliationType getIsAffiliated() {
        return isAffiliated;
    }

    public void setIsAffiliated(AffiliationType isAffiliated) {
        this.isAffiliated = isAffiliated;
    }
}
