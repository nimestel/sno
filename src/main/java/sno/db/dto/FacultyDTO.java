package sno.db.dto;

import sno.db.dto.base.BaseNamedCommentedDTO;

public class FacultyDTO extends BaseNamedCommentedDTO {

    private String dean;

    public String getDean() {
        return dean;
    }

    public void setDean(String dean) {
        this.dean = dean;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        FacultyDTO that = (FacultyDTO) o;

        return dean != null ? dean.equals(that.dean) : that.dean == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (dean != null ? dean.hashCode() : 0);
        return result;
    }
}
