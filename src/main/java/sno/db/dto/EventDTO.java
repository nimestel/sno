package sno.db.dto;

import sno.db.dto.base.BaseCommentedDTO;

import java.util.Date;

public class EventDTO extends BaseCommentedDTO {

    public enum EventType {
        OLYMPICS("Олимпиада"), COMPETITION("Конкурс"), CONFERENCE("Конференция"), SHOW("Выставка"),
        FESTIVAL("Фестиваль"), FORUM("Форум"), SEMINAR("Семинар");

        private String russianName;

        EventType(String russianName) {
            this.russianName = russianName;
        }

        public static EventType fromString(String s) {
            if (s == null) return null;

            switch (s) {
                case "Олимпиада":
                    return OLYMPICS;
                case "Конкурс":
                    return COMPETITION;
                case "Конференция":
                    return CONFERENCE;
                case "Выставка":
                    return SHOW;
                case "Фестиваль":
                    return FESTIVAL;
                case "Форум":
                    return FORUM;
                case "Семинар":
                    return SEMINAR;
                default:
                    return null;
            }
        }

        public String getRussianName() {
            return russianName;
        }
    }

    public enum StatusType {
        INNER("Внутренний"), CITY("Городской"), REGIONAL("Региональный"), CROSS_REGIONAL("Межрегиональный");

        private String russianName;

        StatusType(String russianName) {
            this.russianName = russianName;
        }

        public static StatusType fromString(String s) {
            if (s == null) return null;

            switch (s) {
                case "Внутренний":
                    return INNER;
                case "Городской":
                    return CITY;
                case "Региональный":
                    return REGIONAL;
                case "Межрегиональный":
                    return CROSS_REGIONAL;
                default:
                    return null;
            }
        }

        public String getRussianName() {
            return russianName;
        }
    }

    private EventType eventType;
    private String name;
    private ContactPersonDTO contactPersonDTO;
    private EmployeeDTO employeeDTO;
    private Date beginDate;
    private Date endDate;
    private Date registrationBeginDate;
    private Date registrationEndDate;
    private PrikazDTO prikazProved;
    private PrikazDTO prikazPoochr;
    private StatusType status;
    private Double prize;
    private String link;
    private String file;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        EventDTO eventDTO = (EventDTO) o;

        if (eventType != eventDTO.eventType) return false;
        if (name != null ? !name.equals(eventDTO.name) : eventDTO.name != null) return false;
        if (contactPersonDTO != null ? !contactPersonDTO.equals(eventDTO.contactPersonDTO) : eventDTO.contactPersonDTO != null)
            return false;
        if (employeeDTO != null ? !employeeDTO.equals(eventDTO.employeeDTO) : eventDTO.employeeDTO != null)
            return false;
        if (beginDate != null ? !beginDate.equals(eventDTO.beginDate) : eventDTO.beginDate != null) return false;
        if (endDate != null ? !endDate.equals(eventDTO.endDate) : eventDTO.endDate != null) return false;
        if (registrationBeginDate != null ? !registrationBeginDate.equals(eventDTO.registrationBeginDate) : eventDTO.registrationBeginDate != null)
            return false;
        if (registrationEndDate != null ? !registrationEndDate.equals(eventDTO.registrationEndDate) : eventDTO.registrationEndDate != null)
            return false;
        if (prikazProved != null ? !prikazProved.equals(eventDTO.prikazProved) : eventDTO.prikazProved != null)
            return false;
        if (prikazPoochr != null ? !prikazPoochr.equals(eventDTO.prikazPoochr) : eventDTO.prikazPoochr != null)
            return false;
        if (status != eventDTO.status) return false;
        if (prize != null ? !prize.equals(eventDTO.prize) : eventDTO.prize != null) return false;
        if (link != null ? !link.equals(eventDTO.link) : eventDTO.link != null) return false;
        return file != null ? file.equals(eventDTO.file) : eventDTO.file == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (eventType != null ? eventType.hashCode() : 0);
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (contactPersonDTO != null ? contactPersonDTO.hashCode() : 0);
        result = 31 * result + (employeeDTO != null ? employeeDTO.hashCode() : 0);
        result = 31 * result + (beginDate != null ? beginDate.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (registrationBeginDate != null ? registrationBeginDate.hashCode() : 0);
        result = 31 * result + (registrationEndDate != null ? registrationEndDate.hashCode() : 0);
        result = 31 * result + (prikazProved != null ? prikazProved.hashCode() : 0);
        result = 31 * result + (prikazPoochr != null ? prikazPoochr.hashCode() : 0);
        result = 31 * result + (status != null ? status.hashCode() : 0);
        result = 31 * result + (prize != null ? prize.hashCode() : 0);
        result = 31 * result + (link != null ? link.hashCode() : 0);
        result = 31 * result + (file != null ? file.hashCode() : 0);
        return result;
    }

    public EventType getEventType() {
        return eventType;
    }

    public void setEventType(EventType eventType) {
        this.eventType = eventType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ContactPersonDTO getContactPersonDTO() {
        return contactPersonDTO;
    }

    public void setContactPersonDTO(ContactPersonDTO contactPersonDTO) {
        this.contactPersonDTO = contactPersonDTO;
    }

    public EmployeeDTO getEmployeeDTO() {
        return employeeDTO;
    }

    public void setEmployeeDTO(EmployeeDTO employeeDTO) {
        this.employeeDTO = employeeDTO;
    }

    public Date getBeginDate() {
        return beginDate;
    }

    public void setBeginDate(Date beginDate) {
        this.beginDate = beginDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Date getRegistrationBeginDate() {
        return registrationBeginDate;
    }

    public void setRegistrationBeginDate(Date registrationBeginDate) {
        this.registrationBeginDate = registrationBeginDate;
    }

    public Date getRegistrationEndDate() {
        return registrationEndDate;
    }

    public void setRegistrationEndDate(Date registrationEndDate) {
        this.registrationEndDate = registrationEndDate;
    }

    public PrikazDTO getPrikazProved() {
        return prikazProved;
    }

    public void setPrikazProved(PrikazDTO prikazProved) {
        this.prikazProved = prikazProved;
    }

    public PrikazDTO getPrikazPoochr() {
        return prikazPoochr;
    }

    public void setPrikazPoochr(PrikazDTO prikazPoochr) {
        this.prikazPoochr = prikazPoochr;
    }

    public StatusType getStatus() {
        return status;
    }

    public void setStatus(StatusType status) {
        this.status = status;
    }

    public Double getPrize() {
        return prize;
    }

    public void setPrize(Double prize) {
        this.prize = prize;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
