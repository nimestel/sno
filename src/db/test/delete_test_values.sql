do language plpgsql $$
declare
    id_faculty_var      bigint;
    id_department_var   bigint;
    id__group_var       bigint;
    id_student_var1     bigint;
    id_student_var2     bigint;
    id_prikaz_pr_var1   bigint;
    id_prikaz_po_var1   bigint;
    id_prikaz_pr_var2   bigint;
    id_prikaz_po_var2   bigint;
    id_employee_var1    bigint;
    id_employee_var2    bigint;
    id_organization_var bigint;
    id_cp_var1          bigint;
    id_cp_var2          bigint;
    id_event_var1       bigint;
    id_event_var2       bigint;
begin
    select id_event into id_event_var1 from event where name = 'ТЕСТ_С1';
    select id_event into id_event_var2 from event where name = 'ТЕСТ_С2';

    select id_contact_person into id_cp_var1 from event where id_event = id_event_var1;
    select id_contact_person into id_cp_var2 from event where id_event = id_event_var2;

    select id_employee into id_employee_var1 from event where id_event = id_event_var1;
    select id_employee into id_employee_var2 from event where id_event = id_event_var2;

    select id_prikaz_proved into id_prikaz_pr_var1 from event where id_event = id_event_var1;
    select id_prikaz_proved into id_prikaz_pr_var2 from event where id_event = id_event_var2;
    select id_prikaz_poochr into id_prikaz_po_var1 from event where id_event = id_event_var1;
    select id_prikaz_poochr into id_prikaz_po_var2 from event where id_event = id_event_var2;

    select id_organization into id_organization_var from contact_person where id_contact_person = id_cp_var1;

    select id_student into id_student_var1 from event_participation where id_event = id_event_var1;
    select id_student into id_student_var2 from event_participation where id_event = id_event_var2;

    select id__group into id__group_var from student where id_student = id_student_var1;
    select id_department into id_department_var from _group where id__group = id__group_var;

    select id_faculty into id_faculty_var from department where id_department = id_department_var;

    delete from event_participation where id_event = id_event_var1;
    delete from event_participation where id_event = id_event_var2;
    delete from event where id_event = id_event_var1;
    delete from event where id_event = id_event_var2;
    delete from contact_person where id_contact_person = id_cp_var1;
    delete from contact_person where id_contact_person = id_cp_var2;
    delete from employee where id_employee = id_employee_var1;
    delete from employee where id_employee = id_employee_var2;
    delete from prikaz where id_prikaz = id_prikaz_pr_var1;
    delete from prikaz where id_prikaz = id_prikaz_pr_var2;
    delete from prikaz where id_prikaz = id_prikaz_po_var1;
    delete from prikaz where id_prikaz = id_prikaz_po_var2;
    delete from organization where id_organization = id_organization_var;
    delete from student where id_student = id_student_var1;
    delete from student where id_student = id_student_var2;
    delete from _group where id__group = id__group_var;
    delete from department where id_department = id_department_var;
    delete from faculty where id_faculty = id_faculty_var;
end;
$$;