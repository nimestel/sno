do language plpgsql $$
declare
    id_faculty_var      bigint;
    id_department_var   bigint;
    id__group_var       bigint;
    id_student_var1     bigint;
    id_student_var2     bigint;
    id_prikaz_pr_var1   bigint;
    id_prikaz_po_var1   bigint;
    id_prikaz_pr_var2   bigint;
    id_prikaz_po_var2   bigint;
    id_employee_var1    bigint;
    id_employee_var2    bigint;
    id_organization_var bigint;
    id_cp_var1          bigint;
    id_cp_var2          bigint;
    id_event_var1       bigint;
    id_event_var2       bigint;
begin
    insert into faculty(name, acronym, dean)
        values('Тестовый факультет', 'ТЕСТ_Ф', 'Иван Факультетович Тестов')
	    returning id_faculty into id_faculty_var;
    insert into department(name, acronym, id_faculty, head_of_a_chair)
        values('Тестовая кафедра', 'ТЕСТ_К', id_faculty_var, 'Иван Кафедрович Тестов')
        returning id_department into id_department_var;
    insert into _group(acronym, name, year, id_department)
        values('ТЕСТ_Г', 'Тестовая группа', 3, id_department_var)
        returning id__group into id__group_var;

    insert into student(last_name, first_name, patronymic, gender, birth_date, id__group)
        values('Тестов', 'Тест1', 'Тестович', 'Мужской', '01.08.1993', id__group_var)
        returning id_student into id_student_var1;
    insert into student(last_name, first_name, patronymic, gender, birth_date, id__group)
        values('Тестова', 'Тест2', 'Тестовна', 'Женский', '02.06.1994', id__group_var)
        returning id_student into id_student_var2;

    insert into prikaz(number_prikaz, text_prikaz) values (666001, 'Тестовый приказ о проведении 1')
        returning id_prikaz into id_prikaz_pr_var1;
    insert into prikaz(number_prikaz, text_prikaz) values (666002, 'Тестовый приказ о проведении 2')
        returning id_prikaz into id_prikaz_pr_var2;
    insert into prikaz(number_prikaz, text_prikaz) values (666003, 'Тестовый приказ о поощрении 1')
        returning id_prikaz into id_prikaz_po_var1;
    insert into prikaz(number_prikaz, text_prikaz) values (666004, 'Тестовый приказ о поощрении 2')
        returning id_prikaz into id_prikaz_po_var2;

    insert into employee(last_name, first_name, patronymic, id_department)
        values('Работников', 'Тест1', 'Тестович', id_department_var)
        returning id_employee into id_employee_var1;
    insert into employee(last_name, first_name, patronymic, id_department)
        values('Работников', 'Тест2', 'Тестович', id_department_var)
        returning id_employee into id_employee_var2;

    insert into organization(name, city) values('ТЕСТ_О', 'Киров')
        returning id_organization into id_organization_var;

    insert into contact_person(last_name, first_name, patronymic, gender, id_organization)
        values('Контактов', 'Тест1', 'Тестович', 'Мужской', id_organization_var)
        returning id_contact_person into id_cp_var1;
    insert into contact_person(last_name, first_name, patronymic, gender, id_organization)
        values('Контактова', 'Тест2', 'Тестовна', 'Женский', id_organization_var)
        returning id_contact_person into id_cp_var2;

    insert into event(event_type, name, id_contact_person, id_employee, begin_date, id_prikaz_proved, id_prikaz_poochr)
        values('Конференция', 'ТЕСТ_С1', id_cp_var1, id_employee_var1, '05.11.2015', id_prikaz_pr_var1, id_prikaz_po_var1)
        returning id_event into id_event_var1;
    insert into event(event_type, name, id_contact_person, id_employee, begin_date, id_prikaz_proved, id_prikaz_poochr)
        values('Фестиваль', 'ТЕСТ_С2', id_cp_var2, id_employee_var2, '07.12.2015', id_prikaz_pr_var2, id_prikaz_po_var2)
        returning id_event into id_event_var2;

    insert into event_participation(id_student, id_event, begin_date) values(id_student_var1, id_event_var1, '06.11.2015');
    insert into event_participation(id_student, id_event, begin_date) values(id_student_var2, id_event_var2, '08.12.2015');
end;
$$;