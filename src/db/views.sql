drop view if exists faculty_v cascade;
create view faculty_v as select * from faculty;

drop view if exists department_v cascade;
create view department_v as
select
   d.id_department,
   d.name,
   d.acronym,
   d.id_faculty as faculty_id_faculty,
   f.name as faculty_name,
   f.acronym as faculty_acronym,
   f.dean as faculty_dean,
   f.commentary as faculty_commentary,
   d.head_of_a_chair,
   d.commentary
from department d, faculty f
where d.id_faculty = f.id_faculty;

drop view if exists _group_v cascade;
create view _group_v as
select
    g.id__group,
    g.acronym,
    g.name,
    g.year,
    g.commentary,
    g.id_department as department_id_department,
    d.name as department_name,
    d.acronym as department_acronym,
    d.id_faculty as department_faculty_id_faculty,
    f.name as department_faculty_name,
    f.acronym as department_faculty_acronym,
    f.dean as department_faculty_dean,
    f.commentary as department_faculty_commentary,
    d.head_of_a_chair as department_head_of_a_chair,
    d.commentary as department_commentary
from _group g, department d, faculty f
where g.id_department = d.id_department and f.id_faculty = d.id_faculty;

drop view if exists student_v cascade;
create view student_v as
select
    s.id_student,
    s.last_name,
    s.first_name,
    s.patronymic,
    s.gender,
    s.birth_date,
    s.begin_date,
    s.end_date,
    s.sno_functions,
    s.hobby,
    s.commentary,
    s.id__group as _group_id__group,
    g.acronym as _group_acronym,
    g.name as _group_name,
    g.year as _group_year,
    g.commentary as _group_commentary,
    g.id_department as _group_department_id_department,
    d.name as _group_department_name,
    d.acronym as _group_department_acronym,
    d.id_faculty as _group_department_faculty_id_faculty,
    f.name as _group_department_faculty_name,
    f.acronym as _group_department_faculty_acronym,
    f.dean as _group_department_faculty_dean,
    f.commentary as _group_department_faculty_commentary,
    d.head_of_a_chair as _group_department_head_of_a_chair,
    d.commentary as _group_department_commentary
from student s, _group g, department d, faculty f
where s.id__group = g.id__group and g.id_department = d.id_department and f.id_faculty = d.id_faculty;

drop view if exists prikaz_v cascade;
create view prikaz_v as select * from prikaz;

drop view if exists organization_v cascade;
create view organization_v as select * from organization;

drop view if exists employee_v cascade;
create view employee_v as select
    e.id_employee,
    e.last_name,
    e.first_name,
    e.patronymic,
    e.job,
    e.academic_status,
    e.academic_degree,
    e.commentary,
    e.id_department as department_id_department,
    d.name as department_name,
    d.acronym as department_acronym,
    d.id_faculty as department_faculty_id_faculty,
    f.name as department_faculty_name,
    f.acronym as department_faculty_acronym,
    f.dean as department_faculty_dean,
    f.commentary as department_faculty_commentary,
    d.head_of_a_chair as department_head_of_a_chair,
    d.commentary as department_commentary
from employee e, department d, faculty f
where e.id_department = d.id_department and d.id_faculty = f.id_faculty;

drop view if exists contact_person_v cascade;
create view contact_person_v as select
    cp.id_contact_person,
    cp.last_name,
    cp.first_name,
    cp.patronymic,
    cp.gender,
    cp.job,
    cp.commentary,
    cp.id_organization as organization_id_organization,
    o.name as organization_name,
    o.city as organization_city,
    o.organization_level as organization_organization_level,
    o.organization_type as organization_organization_type,
    o.field as organization_field,
    o.is_affiliated as organization_is_affiliated,
    o.commentary as organization_commentary
from contact_person cp, organization o
where cp.id_organization = o.id_organization;

-- TODO А мы не слишком жирные на 53 колонки?
-- TODO Учитывать nullable поля!
drop view if exists event_v cascade;
create view event_v as select
    e.id_event,
    e.event_type,
    e.name,
    e.begin_date,
    e.end_date,
    e.registration_begin_date,
    e.registration_end_date,
    e.status,
    e.prize,
    e.commentary,
    e.link,
    e.file,
    e.id_contact_person as cp_id_contact_person,
    cp.last_name as cp_last_name,
    cp.first_name as cp_first_name,
    cp.patronymic as cp_patronymic,
    cp.gender as cp_gender,
    cp.job as cp_job,
    cp.commentary as cp_commentary,
    cp.id_organization as cp_organization_id_organization,
    o.name as cp_organization_name,
    o.city as cp_organization_city,
    o.organization_level as cp_organization_organization_level,
    o.organization_type as cp_organization_organization_type,
    o.field as cp_organization_field,
    o.is_affiliated as cp_organization_is_affiliated,
    o.commentary as cp_organization_commentary,
    emp.id_employee as employee_id_employee,
    emp.last_name as employee_last_name,
    emp.first_name as employee_first_name,
    emp.patronymic as employee_patronymic,
    emp.job as employee_job,
    emp.academic_status as employee_academic_status,
    emp.academic_degree as employee_academic_degree,
    emp.commentary as employee_commentary,
    emp.id_department as employee_department_id_department,
    d.name as employee_department_name,
    d.acronym as employee_department_acronym,
    d.id_faculty as employee_department_faculty_id_faculty,
    f.name as employee_department_faculty_name,
    f.acronym as employee_department_faculty_acronym,
    f.dean as employee_department_faculty_dean,
    f.commentary as employee_department_faculty_commentary,
    d.head_of_a_chair as employee_department_head_of_a_chair,
    d.commentary as employee_department_commentary,
    e.id_prikaz_proved as ppr_id_prikaz,
    ppr.number_prikaz as ppr_number_prikaz,
    ppr.date_prikaz as ppr_date_prikaz,
    ppr.text_prikaz as ppr_text_prikaz,
    e.id_prikaz_poochr as ppo_id_prikaz,
    ppo.number_prikaz as ppo_number_prikaz,
    ppo.date_prikaz as ppo_date_prikaz,
    ppo.text_prikaz as ppo_text_prikaz
from event e, contact_person cp, organization o, employee emp, department d, faculty f, prikaz ppr, prikaz ppo
where
    e.id_contact_person = cp.id_contact_person and
    cp.id_organization = o.id_organization and
    e.id_employee = emp.id_employee and
    emp.id_department = d.id_department and
    d.id_faculty = f.id_faculty and
    e.id_prikaz_proved = ppr.id_prikaz and
    e.id_prikaz_poochr = ppo.id_prikaz;

drop view if exists event_participation_v cascade;
create view event_participation_v as select
    ep.begin_date,
    ep.end_date,
    ep.place,
    ep.prize,
    ep.commentary,
    ep.link,
    ep.file,
    ep.id_student as student_id_student,
    s.first_name as student_first_name,
    s.last_name as student_last_name,
    s._group_acronym as student__group_acronym,
    s._group_year as student__group_year,
    ep.id_event as event_id_event,
    e.name as event_name,
    e.status as event_status,
    e.event_type as event_event_type
from event_participation ep, student_v s, event e
where ep.id_student = s.id_student and ep.id_event = e.id_event;

drop view if exists account_v cascade;
create view account_v as select
    a.id_account,
    a.login,
    a.password_hash,
    a.access_level,
    a.email,
    a.info,
    a.id_student as student_id_student,
    s.last_name as student_last_name,
    s.first_name as student_first_name,
    s.patronymic as student_patronymic,
    s._group_acronym as student__group_acronym,
    s._group_year as student__group_year
from account as a
left outer join student_v as s on a.id_student = s.id_student;

drop view if exists discipline_v cascade;
create view discipline_v as select * from discipline;

drop view if exists student_discipline_v cascade;
create view student_discipline_v as select * from student_discipline;

drop view if exists event_discipline_v cascade;
create view event_discipline_v as select * from event_discipline;