create table discipline (
	id_discipline	bigserial primary key,
	name            varchar(100) not null unique
);

create table student_discipline (
    id_student      bigint not null references student(id_student) on delete cascade on update cascade,
    id_discipline   bigint not null references discipline(id_discipline) on delete cascade on update cascade
);

create table event_discipline (
    id_event        bigint not null references event(id_event) on delete cascade on update cascade,
    id_discipline   bigint not null references discipline(id_discipline) on delete cascade on update cascade
);

