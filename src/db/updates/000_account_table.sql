-- Тип для уровня прав доступа
create type access_level_type as enum('Администратор', 'Студент');

-- Аккаунт пользователя.
-- В том случае, если пользователь - студент, может быть связана с одной строкой
-- из таблицы студентов
create table account (
	id_account		bigserial primary key,
	login			varchar(30) not null unique,
	password_hash	char(32) not null,
	access_level	access_level_type not null,
	email			varchar(100),
	info			text,
	id_student      bigint references student(id_student) on update cascade on delete restrict
	    constraint account_access_level_student check (id_student is not null and access_level = 'Студент' or id_student is null)
);

insert into account(login, password_hash, access_level, email, info, id_student) values (
    'admin',
    '202cb962ac59075b964b07152d234b70', -- 123
    'Администратор',
    null, null, null
);